PROJECT=nblmapp

include ${EPICS_ENV_PATH}/module.Makefile

# We need an explicit dependency on nds3epics.
USR_DEPENDENCIES = nds3epics,1.0.0
MISCS   = misc/
OPIS = opi/

USR_CXXFLAGS = -std=c++0x -DIS_EEE_MODULE_NO_TRACE -fpermissive
#USR_CFLAGS = -DIS_EEE_MODULE

INCLUDES += -I../.././src/include

