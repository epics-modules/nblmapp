## nblmapp

This library exposes an interface to IOXOS mTCA DAQ boards with a specific firmware dedicated to the nBLM.
It also provides an EPICS interface thanks to NDS3.

# General information

With the local Makefile in /src, we can also build a standalone executable with no EPICS interface,
data are printed in the terminal.
