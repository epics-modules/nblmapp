#!/bin/bash

# IP IFC140
IP_IFC1410=10.2.176.99
# reboot duration
timeToReboot=40 # sec
# number max of IFC1410 reboot (after that you should try to use the MTCA handle)
maxNumberReboot=20

# check DDR calibration (TscMon)
tscMonLog=/home/iocuser/TscMonLog.txt
tscmonCmd=/home/root/bin/TscMon

rebootCpt=0
# try "maxNumberReboot" times to have no error on IFC1410 boot
while ((rebootCpt<${maxNumberReboot}))
do

((rebootCpt+=1))
echo "Try number before success: $rebootCpt"

echo 
echo Launch Tscmon to check DDR calibration ...
# launch tscmon then exit
ssh root@${IP_IFC1410} "${tscmonCmd} @exit.xpm" > ${tscMonLog}
# analyse ${tscMonLog}
if [ -s ${tscMonLog} ]
then
    # TscMon ok
    echo ... tscmon OK
    break # exit of while
else
    # if empty, means tscmon error
    echo ... tscmon KO, rebooting IFC1410 ${IP_IFC1410} ...
    echo 
    # reboot
    ssh root@${IP_IFC1410} reboot
    # wait that the IFC1410 has reboot
    echo "IFC1410 reboot on going, wait for ${timeToReboot} sec ..."
    sleep ${timeToReboot}
fi
done # end of while ((rebootCpt<${maxNumberReboot}))


if ((rebootCpt==${maxNumberReboot}))
then
    echo
    echo "number max of IFC1410 reboot reached. Please use hardware reboot using the AMC extraction handle"
fi

#Ask for starting the IOC
echo
echo Type: ssh root@${IP_IFC1410}
echo then to run the IOC: iocsh /opt/epics/modules/nblmapp/1.0.0/startup/nblmapp.cmd
