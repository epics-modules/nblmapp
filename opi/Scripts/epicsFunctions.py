import epics    # caget caput
import time     # elapsed time

TIMEOUT_CONNECTION  = 0.1 # sec
TIMEOUT_CAPUT       = 0.1 # sec
TIMEOUT_CAGET       = 0.1 # sec

# status caget / caput
STATUS_IDLE                 = 0
STATUS_OK                   = 1
STATUS_PV_CONNECTION_FAILED = 2
STATUS_TIME_OUT             = 3


def connectPV(pvName):
    """ check connection then return a pv object """
    pv = epics.PV(pvName, connection_timeout=TIMEOUT_CONNECTION)

    # PV not connected
    # if pv.wait_for_connection(timeout=TIMEOUT_CONNECTION) == False:
    #     exit("Fail to connect PV: " + str(pv.pvname))

    return pv

def caput (pv, value, pvReadBack):
    """ 1. check PV connection
        2. caput() 
        3. wait for (pvReadBackValue > (value+1) or pvReadBackValue < (value-1)) # range value in case of rounded number (with floating) 
        return value:
         - 0: idle
         - 1: ok
         - 2: pv connection failed
         - 3: time out caput
    """

    # start = time.time()

    # check connection
    if pv.wait_for_connection(timeout=TIMEOUT_CONNECTION) == False:
        # elpased = time.time()-start
        # print "duration: (%f s)" %(elpased)
        return STATUS_PV_CONNECTION_FAILED

    # blocking caput
    result = pv.put(value=value, wait=True, timeout=TIMEOUT_CAPUT)
    # wait=True: waits until value is effective

    # time out
    if result is None:
        return STATUS_TIME_OUT

    # wait until ReadBack equals value
    # blocking => NO TIMEOUT, COULD BE DANGEROUS !
    # ? timout is done in myCaget() ?
    #YMA 2020/09/16startWhile = time.time()
    #YMA 2020/09/16pvReadBackValue, status = caget(pvReadBack)
    #YMA 2020/09/16while (pvReadBackValue > (value+1) or pvReadBackValue < (value-1)):
    #YMA 2020/09/16    pvReadBackValue, status = caget(pvReadBack)
    #YMA 2020/09/16    time.sleep(0.1) # tempo
    #YMA 2020/09/16    # timeout
    #YMA 2020/09/16    timeElapsed = time.time() - startWhile
    #YMA 2020/09/16    if timeElapsed > 5:
    #YMA 2020/09/16        exit("FAILED: time out while()")
    #YMA 2020/09/16timeElapsed = time.time() - startWhile
    # print "set value == RB value in", timeElapsed, "sec"
    # elapsed time
    # elpased = time.time()-start
    # print "duration: (%f s)" %(elpased)

    return STATUS_OK # caput passed

def caget (pv):
    """ 1. check PV connection
        2. caget 
        return value: value, status
            status:
                - 0: idle
                - 1: ok
                - 2: pv connection failed
                - 3: time out caput
    """
    # start = time.time()

    # init status
    status = STATUS_OK

    # check connection
    if pv.wait_for_connection(timeout=TIMEOUT_CONNECTION) == False:
        # elpased = time.time()-start
        # print "duration: (%f s)" %(elpased)
        return None, STATUS_PV_CONNECTION_FAILED

    # caget
    result = pv.get(timeout=TIMEOUT_CAGET)

    # time out
    if result is None:
        status = STATUS_TIME_OUT

    # elpased = time.time()-start
    # print "duration: (%f s)" %(elpased)

    return result, status

def myCaput(pv, value, pvReadBack):
    """ execute a secured caput. In case of error, push error to status PV before exiting the script"""
    status = caput(pv, value, pvReadBack)
    # status:
    # 0: 
    # 1: ok
    # 2: pv connection failed
    # 3: time out caput
    if status == STATUS_OK:
        # caput went ok
        return True
    else:
        # error
        if status == STATUS_PV_CONNECTION_FAILED:
            exit("PV connection failed: " + str(pv))
        elif status == STATUS_TIME_OUT:
            exit("Time out caput: " + str(pv))

        return False # useless

def myCaget (pv):
    """ secured caget """
    result, status = caget(pv)

    if status != STATUS_OK: 
        exit("PV connection failed or time out caget: " + str(pv))

    return result

######## tests ##############
# need to be uncommented

# def simpleCaputThenCaget(myPv, value):
#     # pv name
#     print "PV: ", myPv.pvname
#     # caput
#     # print "caput:"
#     caput(myPv, value)
#     # caget
#     # print "caget:"
#     caget(myPv)


# startScript = time.time()

# # DEFINE for PV name
# PREFIX          = "IFC1410_nBLM"
# DEVICE          = "PBI-AMC-150"
# DEVICE_TUNING  = "PBI-AMC-130"
# CHANNEL         = "CH0"


# # test witch connected PV
# print "start correct connection .. (%f s)" %(time.time()-startScript)
# myPv = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFCHARGEMPV")
# simpleCaputThenCaget(myPv, -308)
# print "..end correct connection (%f s)\n" %(time.time()-startScript)

# # test witch disconnected PV
# print "start wrong connection .. (%f s)" %(time.time()-startScript)
# myPv = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "_NOT_CONNECTED_PV")
# simpleCaputThenCaget(myPv, -452)
# print "..end wrong connection (%f s)\n" %(time.time()-startScript)


# # direct caput VS secure caput => same time
# myPv = connectPV(PREFIX + ':' + DEVICE + '-' + CHANNEL + "-NBLM_" + "CONFCHARGEMPV")
# value = caget(myPv)
# print "start direct caput same value (not secure).. (%f s)" %(time.time()-startScript)
# start = time.time()
# myPv.put(value=-308, wait=True, timeout=TIMEOUT_CAPUT)
# elpased = time.time()-start
# print "duration: (%f s)" %(elpased)
# print "..end direct caput same value (not secure) (%f s)\n" %(time.time()-startScript)

# value = caget(myPv) + 1
# print "start direct caput new value (not secure).. (%f s)" %(time.time()-startScript)
# start = time.time()
# myPv.put(value=-42, wait=True, timeout=TIMEOUT_CAPUT)
# elpased = time.time()-start
# print "duration: (%f s)" %(elpased)
# print "..end direct caput new value (not secure) (%f s)\n" %(time.time()-startScript)


# print "start direct caget (not secure).. (%f s)" %(time.time()-startScript)
# start = time.time()
# myPv.get()
# elpased = time.time()-start
# print "duration: (%f s)" %(elpased)
# print "..end direct caget (not secure) (%f s)\n" %(time.time()-startScript)
