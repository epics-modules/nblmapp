importPackage(Packages.org.csstudio.opibuilder.scriptUtil);
 
var nblm_scopechannel = PVUtil.getString(pvArray[0]);
 
// create a new macro structure and insert
// the nblm macro with the value of the PV
var nblm_scopemacros = DataUtil.createMacrosInput(true);
nblm_scopemacros.put("CHANNEL", nblm_scopechannel);

widget.setPropertyValue("macros", nblm_scopemacros);

// reload the embedded OPI
widget.setPropertyValue("opi_file", "");
widget.setPropertyValue("opi_file", "scopex.opi");
