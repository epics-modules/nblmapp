#ifndef IFC14_H
#define IFC14_H


#include <iostream>
#include <sstream>

/*
 * Helper macro to push text to user through the INFO pv
 *
 * @param text Has to be something that can be passed to std::string()
 */

#define IFC14NDS_MSGWRN(text)                         \
    do {                                               \
        struct timespec now;                           \
        clock_gettime(CLOCK_REALTIME, &now);           \
        m_infoPV.push(now, std::string(text));                    \
        ndsWarningStream(m_node) << std::string(text) << "\n";    \
    } while(0)

/** Status check helper that prints error message from userspace library,
 * sends a message to the MSGR record and returns ndsError. */
#define IFC14NDS_STATUS_MSGERR(func, status)                      \
    do {                                                           \
        std::ostringstream tmp;                                    \
        tmp << (func) << " " <<                                    \
                    ifcdaqdrv_strerror(status) << (status);        \
        IFC14NDS_MSGWRN(tmp.str());                               \
    } while(0)

#define IFC14NDS_STATUS_CHECK(func, status)                                \
    do {                                                                    \
        if ((status) != status_success) {                                   \
            IFC14NDS_STATUS_MSGERR(func, status);                          \
            /*throw nds::NdsError("ifcdaqdrv returned error");*/            \
        }                                                                   \
    } while (0)


#endif /* IFC14_H */
