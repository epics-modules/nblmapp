#ifndef IFC14AICHANNEL_H
#define IFC14AICHANNEL_H

#include <nds3/nds.h>
#include <nblmdrv.h>
#include "buffers.h"
#include "hdf5_interface.h"

class IFC14AIChannel
{
public:
    IFC14AIChannel(const std::string& name, nds::Node& parentNode, int32_t channelNum, ifcdaqdrv_usr_t &deviceUser);

    void readNeutronCounter(struct neutronCounts &data);
    void readEventDetection(struct eventInfoForArchiving &data);
    void scope(double Index0, uint16_t data0,  uint16_t data1);
    // Functions for Periodic data
    void readDetectorSpecificDataInTnom(struct pedestalInfo &pi);
    void readAccumulatedLossInTnom(struct accumulatedSummary_t &as);
    void readLossInUserDefinedWindow(struct lossInUserDefinedWindow_t &lu);
    void readProtectionFunctionOutputs(struct protectionFunctionOutputs_t &po);
    void readEventStatisticsInTcurr(struct eventStatistics_t &es, int type);

    void setState(nds::state_t newState);
    void getInitialConfig();
    uint64_t sampleRawDataIdx;
    uint64_t sampleEventIdx;
    uint64_t sampleNeutronIdx;
    bool     DODpreview;
    std::shared_ptr<hdf5_for_one_ifc> hdf5Obj;
    double   accumulatedLosses[7] = {0};
    uint8_t displayDiscrimination = 0;
    condRead_t  readNonPeriodicCBchannel;

    std::vector<nds::PVVariableIn<double>> lossOverT;

private:
    // temporary  : because we can not read back the configuration registers
    double   save_Q_TOT_single_neutron;
    double   save_current_trigger_period;
    uint16_t pedestalReg;
    bool     substractPdtal;

//    double maxOfRawDataInCSS;
//    double maxOfEventInCSS;
//    double maxOfNeutronInCSS;
    std::int32_t m_AM_Channel; //Algorithm Module channel

    std::vector<double>  m_double_ED;
    std::vector<int32_t> m_int32_ED0;
    std::vector<int32_t> m_int32_ED1;
    std::vector<int32_t> m_int32_ED2;
    std::vector<int32_t> m_int32_ED3;
    std::vector<int32_t> m_int32_ED4;
    std::vector<int32_t> m_int32_ED5;
    std::vector<int8_t> m_int8_ED0;
    std::vector<int8_t> m_int8_ED1;
    std::vector<int8_t> m_int8_ED2;
    std::vector<int8_t> m_int8_ED3;
    std::vector<int8_t> m_int8_ED4;
    std::vector<int8_t> m_int8_ED5;

    std::vector<double>  m_double_NC;
    std::vector<int32_t> m_int32_NC0;
    std::vector<int32_t> m_int32_NC1;
    std::vector<int32_t> m_int32_NC2;
    std::vector<int32_t> m_int32_NC3;
    std::vector<int32_t> m_int32_NC4;
    std::vector<int32_t> m_int32_NC5;

    std::vector<int32_t> m_int32_scope;
    std::vector<int32_t>  m_axis_scope;

    std::vector<int32_t> andVector(std::vector<int32_t> myVector1, std::vector<int32_t> myVector2);
    std::vector<int32_t> notVector(std::vector<int32_t> myVector);
    ifcdaqdrv_usr_t &m_deviceUser;
    nds::Node m_node;

    void set_eventDetection_thr(const timespec &timespec, const int32_t &value);
    void set_eventDetection_thr2(const timespec &timespec, const int32_t &value);
    void set_inv_of_Q_TOT_single_neutron(const timespec &timespec, const double &value);
    void set_neutronAmpl_min(const timespec &timespec, const int32_t &value);
    void set_neutronTOT_min_indx(const timespec &timespec, const int32_t &value);
    void set_pedestal(const timespec &timespec, const int32_t &value);
    void set_pileupTOT_start_indx(const timespec &timespec, const int32_t &value);
    void set_channel_src_select(const timespec &timespec, const int32_t &value);
    void set_pedestalExcludeEvents(const timespec &timespec, const int32_t &value);
    void set_pedestal_window_start(const timespec &timespec, const int32_t &value);
    void set_pedestal_window_length(const timespec &timespec, const int32_t &value);
    void set_window_start_loss(const timespec &timespec, const int32_t &value);
    void set_window_length_loss(const timespec &timespec, const int32_t &value);
    void set_nominal_trigger_period(const timespec &timespec, const double &value);
    void set_current_trigger_period(const timespec &timespec, const double &value);
    void set_single_neutron_count(const timespec &timespec, const int32_t &value);
    void set_filter0_length(const timespec &timespec, const int32_t &value);
    void set_filter1_length(const timespec &timespec, const int32_t &value);
    void set_x_y_threshold(const timespec &timespec, const double &value);
    void set_ma_0_threshold(const timespec &timespec, const double &value);
    void set_ma_1_threshold(const timespec &timespec, const double &value);
    void set_av_pulse_threshold(const timespec &timespec, const double &value);
    void set_exp_threshold(const timespec &timespec, const double &value);
    void set_filter_x_threshold(const timespec &timespec, const int32_t &value);
    void set_filter_length(const timespec &timespec, const int32_t &value);
    void set_lambda(const timespec &timespec, const double &value);
    void set_beam_permit_LUT(const timespec &timespec, const double &value);
    void set_window_params_wave_start(const timespec &timespec, const int32_t &value);
    void set_window_params_wave_length(const timespec &timespec, const int32_t &value);
    void set_window_params_wave_decimation(const timespec &timespec, const int32_t &value);
    void set_softTrigLevel(const timespec &timespec, const int32_t &value);
    void clr_LossOverT1(const timespec &timespec, const int32_t &value);
    void clr_LossOverT2(const timespec &timespec, const int32_t &value);
    void clr_LossOverT3(const timespec &timespec, const int32_t &value);
    void clr_LossOverT4(const timespec &timespec, const int32_t &value);
    void clr_LossOverT5(const timespec &timespec, const int32_t &value);
    void clr_LossOverT6(const timespec &timespec, const int32_t &value);
    void clr_LossOverT7(const timespec &timespec, const int32_t &value);
    void substractPedestal(const timespec &timespec, const int32_t &value);

    void get_eventDetection_thr(void);
    void get_eventDetection_thr2(void);
    void get_inv_of_Q_TOT_single_neutron(void);
    void get_neutronAmpl_min(void);
    void get_neutronTOT_min_indx(void);
    void get_pedestal(void);
    void get_pileupTOT_start_indx(void);
    void get_channel_src_select(void);
    void get_pedestalExcludeEvents(void);
    void get_pedestal_window_start(void);
    void get_pedestal_window_length(void);
    void get_window_start_loss(void);
    void get_window_length_loss(void);
    void get_nominal_trigger_period(void);
    void get_current_trigger_period(void);
    void get_single_neutron_count(void);
    void get_filter0_length(void);
    void get_filter1_length(void);
    void get_x_y_threshold(void);
    void get_ma_0_threshold(void);
    void get_ma_1_threshold(void);
    void get_av_pulse_threshold(void);
    void get_exp_threshold(void);
    void get_filter_x_threshold(void);
    void get_filter_length(void);
    void get_lambda(void);
    void get_beam_permit_LUT(void);
    void get_window_params_wave_start(void);
    void get_window_params_wave_length(void);
    void get_window_params_wave_decimation(void);

    // Neutron count PVs (from CB6) (scalar data every Monitoring Time Window MTW=1 µs)
    nds::PVVariableIn<std::vector<int32_t> > MTW_n;
    nds::PVVariableIn<std::vector<int32_t> > MTW_q_n;
    nds::PVVariableIn<std::vector<int32_t> > MTW_negSat;
    nds::PVVariableIn<std::vector<int32_t> > MTW_posSat;
    nds::PVVariableIn<std::vector<int32_t> > MTW_Q_bckgnd;
    nds::PVVariableIn<std::vector<int32_t> > MTW_Q_total;
    
    // Event info PVs from CB"channel" [0-5]
    nds::PVVariableIn<std::vector<double> >  MTWindx;
    nds::PVVariableIn<std::vector<int32_t> > Q_TOT;
    nds::PVVariableIn<std::vector<int32_t> > TOT;
    nds::PVVariableIn<std::vector<int32_t> > TOTstartTime;
    nds::PVVariableIn<std::vector<int32_t> > peakTime;
    nds::PVVariableIn<std::vector<int32_t> > peakValue;
    nds::PVVariableIn<std::vector<int32_t> > serialNumber;

    nds::PVVariableIn<std::vector<int8_t> >  TOTlimit;
    nds::PVVariableIn<std::vector<int8_t> >  TOTvalid;
    nds::PVVariableIn<std::vector<int8_t> >  isPart2;
    nds::PVVariableIn<std::vector<int8_t> >  peakValid;
    nds::PVVariableIn<std::vector<int8_t> >  pileUp;
    nds::PVVariableIn<std::vector<int8_t> >  eventValid;

    // Raw data sample (size = SCOPE_RAW_DATA_SAMPLES_MAX) from CB8 (only channel 0 for first version)
    nds::PVVariableIn<std::vector<int32_t> > rawData;
    nds::PVVariableIn<int32_t>               scalarRwData;
    nds::PVVariableIn<std::vector<int32_t> > xAxisRawData;

    // Periodic data in CB7
    nds::PVVariableIn<double>       countT;
    nds::PVVariableIn<int32_t>      beamInT;
    nds::PVVariableIn<double>       neutron_avg;
    nds::PVVariableIn<double>       neutron_min;
    nds::PVVariableIn<double>       neutron_max;
    nds::PVVariableIn<double>       Qbackground_avg;
    nds::PVVariableIn<double>       Qbackground_min;
    nds::PVVariableIn<double>       Qbackground_max;
    nds::PVVariableIn<double>       QtotalAvg;
    nds::PVVariableIn<double>       QtotalMin;
    nds::PVVariableIn<double>       QtotalMax;
    nds::PVVariableIn<double>       Qneutron_avg;
    nds::PVVariableIn<double>       Qneutron_min;
    nds::PVVariableIn<double>       Qneutron_max;
    nds::PVVariableIn<double>       loss_over_beamON;
    nds::PVVariableIn<double>       loss_over_pulse;
    
    nds::PVVariableIn<std::int32_t> Tnom_num_of_samples;
    nds::PVVariableIn<double>       Tnom_sum_of_samples;
    nds::PVVariableIn<double>       Tnom_sum_of_squares;
    nds::PVVariableIn<std::int32_t> Tnom_negative_saturations;
    nds::PVVariableIn<std::int32_t> Tnom_positive_saturations;
    nds::PVVariableIn<double>       Tnom_pedestal;
    nds::PVVariableIn<double>       Tnom_rms_noise;

    nds::PVVariableIn<double>               lossDecimation_factor;
    nds::PVVariableIn<double>               lossWindow_position;
    nds::PVVariableIn<double>               lossWindow_samples;
    nds::PVVariableIn<double>               lossWords;
    nds::PVVariableIn<std::vector<double> > lossData_avg;
    nds::PVVariableIn<std::vector<double> > lossData_max;

    nds::PVVariableIn<double>               protectionDecimation_factor;
    nds::PVVariableIn<double>               protectionWindow_position;
    nds::PVVariableIn<double>               protectionWindow_samples;
    nds::PVVariableIn<double>               protectionWords;
	nds::PVVariableIn<std::vector<double> > protAvg0Max;
	nds::PVVariableIn<std::vector<double> > protAvg1Max;
    nds::PVVariableIn<std::vector<double> > protRelaxMax;
	nds::PVVariableIn<std::vector<double> > protXY_count;

    nds::PVVariableIn<std::int32_t> Tcurr_single_TOTmin;
    nds::PVVariableIn<std::int32_t> Tcurr_single_TOTmax;
    nds::PVVariableIn<double>       Tcurr_single_TOTaverage;
    nds::PVVariableIn<double>       Tcurr_single_TOTrms;
    nds::PVVariableIn<std::int32_t> Tcurr_single_peakValueMin;
    nds::PVVariableIn<std::int32_t> Tcurr_single_peakValueMax;
    nds::PVVariableIn<double>       Tcurr_single_peakValueAverage;
    nds::PVVariableIn<double>       Tcurr_single_peakValueRms;
    nds::PVVariableIn<std::int32_t> Tcurr_single_Q_TOTmin;
    nds::PVVariableIn<std::int32_t> Tcurr_single_Q_TOTmax;
    nds::PVVariableIn<double>       Tcurr_single_Q_TOTaverage;
    nds::PVVariableIn<double>       Tcurr_single_Q_TOTrms;
    nds::PVVariableIn<std::int32_t> Tcurr_single_eventCount;

    nds::PVVariableIn<std::int32_t> Tcurr_pileUp_TOTmin;
    nds::PVVariableIn<std::int32_t> Tcurr_pileUp_TOTmax;
    nds::PVVariableIn<double>       Tcurr_pileUp_TOTaverage;
    nds::PVVariableIn<double>       Tcurr_pileUp_TOTrms;
    nds::PVVariableIn<std::int32_t> Tcurr_pileUp_peakValueMin;
    nds::PVVariableIn<std::int32_t> Tcurr_pileUp_peakValueMax;
    nds::PVVariableIn<double>       Tcurr_pileUp_peakValueAverage;
    nds::PVVariableIn<double>       Tcurr_pileUp_peakValueRms;
    nds::PVVariableIn<std::int32_t> Tcurr_pileUp_Q_TOTmin;
    nds::PVVariableIn<std::int32_t> Tcurr_pileUp_Q_TOTmax;
    nds::PVVariableIn<double>       Tcurr_pileUp_Q_TOTaverage;
    nds::PVVariableIn<double>       Tcurr_pileUp_Q_TOTrms;
    nds::PVVariableIn<std::int32_t> Tcurr_pileUp_eventCount;

    nds::PVVariableIn<std::int32_t> Tcurr_allEvt_TOTmin;
    nds::PVVariableIn<std::int32_t> Tcurr_allEvt_TOTmax;
    nds::PVVariableIn<double>       Tcurr_allEvt_TOTaverage;
    nds::PVVariableIn<double>       Tcurr_allEvt_TOTrms;
    nds::PVVariableIn<std::int32_t> Tcurr_allEvt_peakValueMin;
    nds::PVVariableIn<std::int32_t> Tcurr_allEvt_peakValueMax;
    nds::PVVariableIn<double>       Tcurr_allEvt_peakValueAverage;
    nds::PVVariableIn<double>       Tcurr_allEvt_peakValueRms;
    nds::PVVariableIn<std::int32_t> Tcurr_allEvt_Q_TOTmin;
    nds::PVVariableIn<std::int32_t> Tcurr_allEvt_Q_TOTmax;
    nds::PVVariableIn<double>       Tcurr_allEvt_Q_TOTaverage;
    nds::PVVariableIn<double>       Tcurr_allEvt_Q_TOTrms;
    nds::PVVariableIn<std::int32_t> Tcurr_allEvt_eventCount;

    nds::PVVariableIn<std::int32_t> Tcurr_bckgnd_TOTmin;
    nds::PVVariableIn<std::int32_t> Tcurr_bckgnd_TOTmax;
    nds::PVVariableIn<double>       Tcurr_bckgnd_TOTaverage;
    nds::PVVariableIn<double>       Tcurr_bckgnd_TOTrms;
    nds::PVVariableIn<std::int32_t> Tcurr_bckgnd_peakValueMin;
    nds::PVVariableIn<std::int32_t> Tcurr_bckgnd_peakValueMax;
    nds::PVVariableIn<double>       Tcurr_bckgnd_peakValueAverage;
    nds::PVVariableIn<double>       Tcurr_bckgnd_peakValueRms;
    nds::PVVariableIn<std::int32_t> Tcurr_bckgnd_Q_TOTmin;
    nds::PVVariableIn<std::int32_t> Tcurr_bckgnd_Q_TOTmax;
    nds::PVVariableIn<double>       Tcurr_bckgnd_Q_TOTaverage;
    nds::PVVariableIn<double>       Tcurr_bckgnd_Q_TOTrms;
    nds::PVVariableIn<std::int32_t> Tcurr_bckgnd_eventCount;

    nds::PVVariableIn<std::int32_t> evtThr;
    nds::PVVariableIn<std::int32_t> evtThr2;
    nds::PVVariableIn<double>       QTOT_n_set;
    nds::PVVariableIn<std::int32_t> n_AmpMin;
    nds::PVVariableIn<std::int32_t> n_TOTMinIdx;
    nds::PVVariableIn<std::int32_t> pedestal;
    nds::PVVariableIn<std::int32_t> PU_TOTStart;
    nds::PVVariableIn<std::int32_t> channelSrc;
    nds::PVVariableIn<std::int32_t> pdstlNoEvt;
    nds::PVVariableIn<std::int32_t> pdstlWStart;
    nds::PVVariableIn<std::int32_t> pdstlWSize;
    nds::PVVariableIn<std::int32_t> WStartLoss;
    nds::PVVariableIn<std::int32_t> WSizeLoss;
    nds::PVVariableIn<double>       nomTrigger;
    nds::PVVariableIn<double>       currentTriggerPeriod;
    nds::PVVariableIn<std::int32_t> n_Cnt;
    nds::PVVariableIn<std::int32_t> filtr0Size;
    nds::PVVariableIn<std::int32_t> filtr1Size;
    nds::PVVariableIn<double>       xyThr;
    nds::PVVariableIn<double>       ma0Thr;
    nds::PVVariableIn<double>       ma1Thr;
    nds::PVVariableIn<double>       avPulseThr;
    nds::PVVariableIn<double>       expThr;
    nds::PVVariableIn<std::int32_t> filterxThr;
    nds::PVVariableIn<std::int32_t> filterSize;
    nds::PVVariableIn<double>       lambda;
    nds::PVVariableIn<double>       beamPrmitLUT;
    nds::PVVariableIn<std::int32_t> WWaveStart;
    nds::PVVariableIn<std::int32_t> WWaveSize;
    nds::PVVariableIn<std::int32_t> WWaveDecim;
    nds::PVVariableIn<std::int32_t> data_channel;
    
    uint8_t refreshDetectorSpecificDataInTnom = 0;
    uint8_t refreshAccumulatedLossInTnom = 0;
    uint8_t refreshLossInUserDefinedWindow = 0;
    uint8_t refreshProtectionFunctionOutputs = 0;
    uint8_t refreshEventStatisticsInTcurr = 0;
    bool    trig = false;
    uint16_t softTrigLevel = 0;
};

#endif /* IFC14AICHANNEL_H */
