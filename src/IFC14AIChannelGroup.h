/**
 * @file IFC14AIChannelGroup.h
 * @brief Header file defining the analog input channel group class.
 * @author yannickmariette-CEA-Saclay-FRANCE
 * @date 2018-12-19
 *
 * Adaptation made for the specific nBLM application
 * Code based on ifcdaq  
 *
 */

#ifndef IFC14AICHANNELGROUP_H
#define IFC14AICHANNELGROUP_H

#include <nds3/nds.h>
#include <chrono>


#include "IFC14AIChannel.h"
#include "data_processor.h"


/**
 * @brief IFC14AIChannelGroup
 */

long elapsed_time(timespec start, timespec end);

class IFC14AIChannelGroup {
public:
    IFC14AIChannelGroup(const std::string& name, nds::Node& parentNode, ifcdaqdrv_usr_t &deviceUser);

    nds::Port m_node;
    ifcdaqdrv_usr_t& m_deviceUser;
    uint32_t channelMask=0;
    bool MainNotStarted = true;
    bool resetChannels = false;
    nds::Thread              m_epicsNonPeriodicThread;
    nds::Thread              m_epicsPeriodicConsumerThread;
    std::vector<nds::Thread> m_consumerThread;
    std::vector<nds::Thread> m_periodicConsumerThread;
    std::vector<std::thread> m_epicsInterleaverThread;
    std::vector<nds::Thread> m_epicsAccumulatedLossThread;

    /* Class parameter that holds analog channels objects */
    std::vector<std::shared_ptr<IFC14AIChannel> > m_AIChannels;
    /* Class parameter that holds Circular Buffer channels objects */
    std::vector<std::shared_ptr<DataProcessor> > processors;
    
    void processNonPeriodicData(void);
    void processPeriodicData(void);
    void processDOD(uint8_t channelNb, uint16_t timeout_ms) {processors.at(channelNb)->processOfflineDataInterleaved(timeout_ms);};
    void accumulatedLossThread(const uint8_t T);
    

    void getnBLM_versions();

    bool allowChange(const nds::state_t currentLocal, const nds::state_t currentGlobal, const nds::state_t nextLocal);
    
    void allocateBuffers(void);
    void freeBuffers();
    void copyDataStream(int ch, int t);
    void buffer_checker (int t);
    void main_loop (int t);

private:
    pthread_mutex_t  reader_mutex[2];
    pthread_cond_t   condvar[2];    

    bool firstReconf = true;
    char ACT_drv_version[10];
    char ACT_drv_date[30];
    char ACT_epicsDrv_version[10];
    char ACT_epicsDrv_date[30];
    nds::PVVariableIn<double> m_testCounterPV;
    nds::PVVariableIn<std::int32_t> collectOn;
    nds::PVVariableIn<std::string> m_nBLM_infoVersionsPV;
    bool DODpreview;
    nds::PVVariableIn<std::int32_t> channel_mask;
    nds::PVVariableIn<double> decimStart;
    nds::PVVariableIn<double> decimStop;
    nds::PVVariableIn<std::int32_t> T1;
    nds::PVVariableIn<std::int32_t> T2;
    nds::PVVariableIn<std::int32_t> T3;
    nds::PVVariableIn<std::int32_t> T4;
    nds::PVVariableIn<std::int32_t> T5;
    nds::PVVariableIn<std::int32_t> T6;
    nds::PVVariableIn<std::int32_t> T7;
    nds::PVVariableIn<std::int32_t> periodicDisplayFrequency;
    
    nds::Thread m_mainThread;
    nds::Thread consumer_thread0;
    nds::Thread consumer_thread1;
    nds::Thread dma_thread0;
    nds::Thread dma_thread1;

    condRead_t readNonPeriodicCB;
    std::condition_variable cv[LOSS_PERIOD_NB];
    std::mutex mtxCounter[LOSS_PERIOD_NB];
    volatile bool exitSleep[LOSS_PERIOD_NB] = {false};

    int        main_thread(void);

    uint32_t get_channel_mask(void);
    
//    void    stop_all_CB_channels(void);
    void    reconfigure_CB_channels(void);
    void    initialize_channel_arrays (void);
    void    set_CBs(const timespec &timespec, const int32_t &value);
    void    set_enableCB0(const timespec &timespec, const int32_t &value);
    void    set_enableCB1(const timespec &timespec, const int32_t &value);
    void    set_enableCB2(const timespec &timespec, const int32_t &value);
    void    set_enableCB3(const timespec &timespec, const int32_t &value);
    void    set_enableCB4(const timespec &timespec, const int32_t &value);
    void    set_enableCB5(const timespec &timespec, const int32_t &value);
    void    set_enableCB6(const timespec &timespec, const int32_t &value);
    void    set_enableCB7(const timespec &timespec, const int32_t &value);
    void    set_enableCB8(const timespec &timespec, const int32_t &value);
    void    set_enableCB9(const timespec &timespec, const int32_t &value);
    void    set_enableCB10(const timespec &timespec, const int32_t &value);
    void    set_enableCB11(const timespec &timespec, const int32_t &value);
    void    set_enableCB12(const timespec &timespec, const int32_t &value);
    void    set_enableCB13(const timespec &timespec, const int32_t &value);
    void    set_configureCBs(const timespec &timespec, const int32_t &value);
    void    set_startDODreq(const timespec &timespec, const int32_t &value);
    void    set_stopDODreq(const timespec &timespec, const int32_t &value);
    void    set_DODpreview(const timespec &timespec, const int32_t &value);
    void    set_decimStart(const timespec &timespec, const double &value);
    void    set_decimStop(const timespec &timespec, const double &value);
    void    set_T1(const timespec &timespec, const int32_t &value);
    void    set_T2(const timespec &timespec, const int32_t &value);
    void    set_T3(const timespec &timespec, const int32_t &value);
    void    set_T4(const timespec &timespec, const int32_t &value);
    void    set_T5(const timespec &timespec, const int32_t &value);
    void    set_T6(const timespec &timespec, const int32_t &value);
    void    set_T7(const timespec &timespec, const int32_t &value);
    void    set_periodicDisplayFrequency(const timespec &timespec, const int32_t &value);
    void    set_fillFiles (void) {for(size_t i(0); i < CB_CHANNEL_NB_MAX; ++i) processors.at(i)->fillFile = true;};
    void    get_decimStart(void);
    void    get_decimStop(void);
    void    get_T1(void);
    void    get_T2(void);
    void    get_T3(void);
    void    get_T4(void);
    void    get_T5(void);
    void    get_T6(void);
    void    get_T7(void);
    void    clr_fillFiles (void) {for(size_t i(0); i < CB_CHANNEL_NB_MAX; ++i) processors.at(i)->fillFile = false;};
    void    exitNeutronCounter(uint8_t counterNb) {std::lock_guard<std::mutex> lck(mtxCounter[counterNb]); exitSleep[counterNb]= true; cv[counterNb].notify_all();}
    void    set_exit_processData_loop(void) {exit_processData_loop= true; for(size_t i(0); i < LOSS_PERIOD_NB; ++i) exitNeutronCounter(i); for(size_t i(0); i < CB_CHANNEL_NB_MAX; ++i) processors.at(i)->exit_processData = true;};
    void    clr_exit_processData_loop(void) {exit_processData_loop= false; for(size_t i(0); i < CB_CHANNEL_NB_MAX; ++i) processors.at(i)->exit_processData = false;};
    void    set_previewAndNoHDF5 (void) {DODpreview= true; for(size_t i(0); i < AM_CHANNEL_NB_MAX; ++i) m_AIChannels.at(i)->DODpreview= true;};
    void    clr_previewAndNoHDF5 (void) {DODpreview= false; for(size_t i(0); i < AM_CHANNEL_NB_MAX; ++i) m_AIChannels.at(i)->DODpreview= false;};
    void    set_displayDiscrimination(void) {for(size_t i(0); i < AM_CHANNEL_NB_MAX; ++i) m_AIChannels.at(i)->displayDiscrimination= displayDiscrimination;};

    int printout_decimator;
    uint32_t read_addr[CB_CHANNEL_NB_MAX];
    size_t bufsizes[CB_CHANNEL_NB_MAX];
    int stalled_buffer[CB_CHANNEL_NB_MAX]; // which buffer was stalled 0-none, 1 - buffer0, 2 - buffer1
    int size_g[2][CB_CHANNEL_NB_MAX];
    int size_r[2][CB_CHANNEL_NB_MAX];
    int channels_active[2][2];
    volatile int current_buffer[2];
    volatile int checked_buffer[2] = {0};
    volatile bool exit_processData_loop;
    uint32_t buf_beg[CB_CHANNEL_NB_MAX];
    uint32_t buf_end[CB_CHANNEL_NB_MAX];
    uint16_t lossPeriod[LOSS_PERIOD_NB] = {0};
    uint8_t  displayDiscrimination = 0;
};
#endif /* IFCDAQAICHANNELGROUP_H */
