#ifndef IFC14DEVICE_H
#define IFC14DEVICE_H

#include <nds3/nds.h>

#include <nblmdrv.h>

#include "IFC14AIChannelGroup.h"
#include "IFC14AIChannel.h"

class IFC14Devices
{
  public:
    IFC14Devices(nds::Factory& factory, const std::string &deviceName, const nds::namedParameters_t& parameters);
    ~IFC14Devices();

  private:
    std::vector<std::shared_ptr<IFC14AIChannelGroup> > m_AIChannelGroups;
    //std::vector<ifcdaqdrv_usr_t> m_deviceUser;
    nds::Node m_node;
};

#endif /* IFC14DEVICE_H */
