#ifndef _TOSCA_MON_H_
#define _TOSCA_MON_H_

typedef int ComFunction ();

/* A structure which contains information on the commands this program can understand. */
typedef struct {
	char		*name;		/* User printable name of the function. */
	ComFunction	*func;		/* Function to call to do the job. */
	char		**doc;		/* Documentation for this function.  */
	char		*summary;	/* Short summary for this function.  */
} COMMAND;


void history_show(int howMany, char *filter);

extern int isInteractive();
extern int getNextInputChar();

extern COMMAND commands[];
extern int programDone;
extern int revertInput(int andexit);
extern int execute_cmd(char *line);

#endif
