#include <stdio.h>
#include "data_source.h"
#include "evdet.h"
#include "hls_stream.h"
#include <iostream>
#ifndef __SYNTHESIS__
#include <fstream>
#endif
#include "algo_prepare_reference.h"

#include <vector>
using namespace std;

ofstream CB1;

vector<eventInfoForArchiving> eventList;
vector<neutronCounts> neutronList;
vector<preprocessedInfo> sampleList;
bool doneOnce=false;

void prepare_reference()
{
//uint32_t data;
eventInfo E, New, Old;
preprocessedData PD;
neutronCounts nc;
eventInfoForArchiving es;
preprocessedInfo ss;

if(doneOnce)
	return;
else
	doneOnce = true;

CB1.open("CB1.txt");
if (!CB1.is_open())
{
    printf("\nCannot open CB1.txt !\n\n");
    throw("Cannot open CB1.txt");
}

float inverse_of_Q_TOT_single_neutron = 100.0f /*want results in one-hundredths of neutron */ /(3.0f*65535.0f);

const ap_fixed<17,17> neutronAmpl_min = -3604;
const uint16_t neutronTOT_min_indx = 14;
const uint16_t pileUpTOT_start_indx = 75;
const ap_fixed<17,17> eventDetection_thr = -1638;
const ap_fixed<17,17> eventDetection_thr2 = -983;
//const uint16_t pedestal = 32768 + 3276; // 0.05*65535;
const uint16_t pedestal = 32768;

//int event_no = 0;

for(int i=0; i<125*MTW_PERIODS_IN_TEST_PATTERN; ++i) {

	hls::stream<sampleAndTimestamp> data_stream("data_stream");
	hls::stream<preprocessedInfo> sample_stream("sample_stream");
	hls::stream<eventInfoForArchiving> event_stream("event_stream");
	hls::stream<neutronCounts> nc2_stream("nc2_stream");
	hls::stream<neutronCounts> nc3_stream("nc3_stream");
	hls::stream<neutronCounts> nc4_stream("nc4_stream");
	hls::stream<neutronCounts> nc5_stream("nc5_stream");
	hls::stream<neutronCounts> nc6_stream("nc6_stream");

#ifdef PUT_TIMESTAMP
	datagen(data_stream, 5*250, true);
#else
	datagen(data_stream, 5*250, false);
#endif

	hls::stream<preprocessedData> PP_stream("PP_stream");
	hls::stream<eventInfo> E_stream("E_stream");
	hls::stream<eventInfo> E2_stream("E2_stream");
	hls::stream<eventInfo> New_stream("New_stream");
	hls::stream<eventInfo> Old_stream("Old_stream");
	hls::stream<neutronCounts> nc_stream("nc_stream");
	hls::stream<pedestalComputationData> PC_stream("Pedestal_stream");

	preprocess (data_stream, PP_stream, sample_stream, neutronAmpl_min,
	eventDetection_thr, eventDetection_thr2, pedestal);
	detect (PP_stream, E_stream, E2_stream, PC_stream, event_stream, neutronTOT_min_indx, pileUpTOT_start_indx);
	neutron_aligner(E_stream, New_stream, Old_stream);
	neutron_counter (New_stream, Old_stream, nc_stream, inverse_of_Q_TOT_single_neutron);
	neutron_summarizer (nc_stream, nc2_stream, nc3_stream, nc4_stream, nc5_stream, nc6_stream);

	if (!nc2_stream.empty())
	{
		nc2_stream >> nc;
		if (nc.newFrame)
			neutronList.push_back(nc);
	}

	if (!nc3_stream.empty())
	{
		nc3_stream.read();
	}

	if (!nc4_stream.empty())
	{
		nc4_stream.read();
	}

	if (!nc5_stream.empty())
	{
		nc5_stream.read();
	}

	if (!nc6_stream.empty())
	{
		nc6_stream.read();
	}

	if (!E2_stream.empty())
	{
		E2_stream.read();
	}


	if (!sample_stream.empty())
	{
		sample_stream >> ss;
		sampleList.push_back(ss);
//		cout << ss.frame_index << " - " << (uint32_t)ss.sample_index;
//		printf("- %x %x\n", ss.sample1, ss.sample0); 
	}
		

	if (!event_stream.empty())
	{
		event_stream >> es;
		if (es.eventValid)
			eventList.push_back(es);
	}

	if (!PC_stream.empty())
	   PC_stream.read();

	
}
#ifndef IS_EEE_MODULE_NO_TRACE
  cout << eventList.size() << " events" << endl;
#endif
}

