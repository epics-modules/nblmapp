/*caMonitor.c*/

/* This example accepts the name of a file containing a list of pvs to monitor.
 * It prints a message for all ca events: connection, access rights and monitor.
 */

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "cadef.h"
#include "dbDefs.h"
#include "epicsString.h"
#include "cantProceed.h"

#include "buffers.h"
#include <thread>
#include <inttypes.h>
#include "interleaver_thread.h"
#include <time.h>

#define MAX_PV 1
#define MAX_PV_NAME_LEN 40

typedef struct{
    char		value[20];
    chid		mychid;
    evid		myevid;
    CircularBufferBlocking<evt_timestamp_pair>* buffers;
} caData;


static void printChidInfo(chid chid, const char *message)
{
    printf("\n%s\n",message);
    printf("pv: %s  type(%d) nelements(%ld) host(%s)",
	ca_name(chid),ca_field_type(chid),ca_element_count(chid),
	ca_host_name(chid));
    printf(" read(%d) write(%d) state(%d)\n",
	ca_read_access(chid),ca_write_access(chid),ca_state(chid));
}

static void exceptionCallback(struct exception_handler_args args)
{
    chid	chid = args.chid;
    long	stat = args.stat; /* Channel access status code*/
    const char  *channel;
    static const char *noname = "unknown";

    channel = (chid ? ca_name(chid) : noname);


    if(chid) printChidInfo(chid,"exceptionCallback");
    printf("exceptionCallback stat %s channel %s\n",
        ca_message(stat),channel);
}

static void connectionCallback(struct connection_handler_args args)
{
    chid	chid = args.chid;

    printChidInfo(chid,"connectionCallback");
}

static void accessRightsCallback(struct access_rights_handler_args args)
{
    chid	chid = args.chid;

    printChidInfo(chid,"accessRightsCallback");
}
static void eventCallback(struct event_handler_args eha)
{
    chid	chid = eha.chid;

    if(eha.status!=ECA_NORMAL)
    {
	    printChidInfo(chid,"eventCallback");
    }
    else
    {
        char	*pdata = (char *)eha.dbr;
        //printf("Event Callback: %s = %s ",ca_name(eha.chid),pdata);

        uint32_t nanoseconds;
        //uint32_t seconds;

    // Date format : "2020-02-28 16:26:15.961939467"
    struct tm myDate;
    uint32_t years, months;
    sscanf(pdata, "%d" "-%d" "-%d" "%d" ":%d" ":%d" ".%" PRIu32, &years, &months, &myDate.tm_mday, &myDate.tm_hour, &myDate.tm_min, &myDate.tm_sec, &nanoseconds);
    myDate.tm_year = years - 1900;
    myDate.tm_mon = months - 1;
    myDate.tm_isdst = -1;   // unknown
    // Calculate time in second since Epoch, 1970-01-01 00:00:00 +0000 (UTC) 
    time_t timestamp_second = mktime( & myDate );
    //printf( "Timestamp_second == %d\n", timestamp_second );
    //uint64_t timestamp = timestamp_second*1000000000ull + nanoseconds;
//    const char * strDate = asctime( localtime( & timestamp_second ) );
//    printf( "timestamp_second time == %s\n", strDate );
    struct evt_timestamp_pair evt_stamp;
    evt_stamp.e.seconds=timestamp_second; //since Epoch
    evt_stamp.e.nanoseconds=nanoseconds;

// If string format : current seconds.nanoseconds of the date
//        sscanf(pdata, "%" PRIu32 ".%" PRIu32, &seconds, &nanoseconds);
//        printf("seconds=%" PRIu32 " nanoseconds=%" PRIu32 " ", seconds, nanoseconds);
//        uint64_t timestamp = seconds*1000000000ull + nanoseconds;

//        static uint64_t previous_timestamp;
//        if(previous_timestamp)
//            printf("Timestamp delta=%" PRIu64 "\n", timestamp - previous_timestamp);
//        previous_timestamp = timestamp;
//        struct evt_timestamp_pair evt_stamp;
//        evt_stamp.e.seconds=seconds;
//        evt_stamp.e.nanoseconds=nanoseconds;

        gettimeofday(&evt_stamp.c, (struct timezone *) 0);
#if 1
        CircularBufferBlocking<evt_timestamp_pair>* cbptr=((caData*)eha.usr)->buffers;
        size_t size_written;
        cbptr->put_data(&evt_stamp, 1, size_written);
        if (size_written!=1)
            printf("Unable to put evt timestamp into FIFO\n");
#endif
    }
}

void camonitorProcessingThread(CircularBufferBlocking<evt_timestamp_pair>* cb_evt, volatile bool& exitThread)
try
{
    int		npv = 0; 
    caData	*pmynode[MAX_PV];
    char	*pname[MAX_PV];
    int		i;
 
    printf("Starting camonitorProcessingThread task\n");

    npv=1;

    pname[0] = epicsStrDup("MTCA-EVR:TSstringin");


    pmynode[0] = (caData*)callocMustSucceed(1, sizeof(caData), "caMonitor");
    
    pmynode[0]->buffers = &cb_evt[0];
    
    SEVCHK(ca_context_create(ca_enable_preemptive_callback),"ca_context_create");
    SEVCHK(ca_add_exception_event(exceptionCallback,NULL),"ca_add_exception_event");
    for (i=0; i<npv; i++)
    {
	SEVCHK(ca_create_channel(pname[i],connectionCallback,pmynode[i],20,&pmynode[i]->mychid),"ca_create_channel");
	SEVCHK(ca_replace_access_rights_event(pmynode[i]->mychid,accessRightsCallback),"ca_replace_access_rights_event");
	SEVCHK(ca_create_subscription(DBR_STRING,1,pmynode[i]->mychid,DBE_VALUE,eventCallback,pmynode[i],&pmynode[i]->myevid),"ca_create_subscription");
    }
    /*Should never return from following call*/
    while((!exit_loop) && (!exitThread))
      ca_pend_event(1.0); // The send buffer is flushed and CA background activity is processed for 1 second

    SEVCHK(ca_clear_subscription (pmynode[i]->myevid),"ca_clear_subscription");
    
    printf("End of camonitorProcessingThread task\n");
    return;
} catch(...)
{
  printf("Exception in camonitorProcessingThread()\n");
}


