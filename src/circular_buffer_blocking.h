#ifndef __CIRCULAR_BUFFER_BLOCKING_H__
#define  __CIRCULAR_BUFFER_BLOCKING_H__
//#define VERBOSE_TEST
#include <mutex>
#include <vector>
#include <condition_variable>
#include <string.h>
#ifdef VERBOSE_TEST
#include <iostream>
#endif

template<class T>  class CircularBufferBlocking
{
private:
  std::mutex m;
  std::condition_variable c;
  size_t write_index;
  size_t read_index ; // write_index == read_index - empty
  bool   bufferHasLooped = false;
public:
  CircularBufferBlocking(size_t size = 0) : buffer(size)
  {
    write_index = 0 ;
    read_index  = 0 ;
  };

  std::vector<T> buffer;

  void put_data(T* src, size_t size, size_t& size_written)
  {
#ifdef VERBOSE_TEST
    std::cout << "Type= "<< typeid(T).name() << " Request to write " << size << " items" << std::endl;
    if(size)
    {
      // for(size_t i=0; i<size; ++i)
      //   std::cout << ((char*)src)[i];
      // std::cout << std::endl;
    }
#endif
    if (size == 0)
      return;
    bool twoparts = false;
    size_t size1 = size;
    size_t size2 = 0;
    size_t size_written_2 = 0;

    {
      std::unique_lock<std::mutex> l(m);
      if (write_index + size > buffer.size()) // assume no integer overflow
        {
          twoparts = true;
          size1 = buffer.size() - write_index;
          size2 = size - size1;
        }
      if (read_index > write_index && write_index + size1 >= read_index) 
         size_written = read_index - write_index - 1;
      else if (write_index + size1 == buffer.size() && read_index == 0)
        size_written = buffer.size() - write_index - 1;
      else
        size_written = size1;

#ifdef VERBOSE_TEST
      std::cout << "Type= "<< typeid(T).name() << " Writing (1) " << size_written*sizeof(T) << " bytes at " << write_index << std::endl;
      if(size_written)
      {
  //      for(size_t i=write_index; i<write_index + size_written; ++i)
  //        std::cout << buffer.data();
  //      std::cout << std::endl;
      }
#endif
    }
    memcpy(&buffer[write_index], src, size_written*sizeof(T));


    {
      std::unique_lock<std::mutex> l(m);
      write_index += size_written;
      if(write_index == buffer.size())
        write_index = 0;
      if (size_written != size1)
        twoparts = 0;

      if (twoparts) 
      {
        if (read_index > write_index && write_index + size2 >= read_index) 
          size_written_2 = read_index - write_index - 1;
        else if (write_index + size2 == buffer.size() && read_index == 0)
          size_written_2 = buffer.size() - write_index - 1;
        else
          size_written_2 = size2;
      }
      c.notify_one();
    }
    
    if (twoparts)
    {
      memcpy(&buffer[write_index], (uint8_t*)src + size1, size_written_2*sizeof(T));
      {
        std::unique_lock<std::mutex> l(m);
#ifdef VERBOSE_TEST
        std::cout << "Type= "<< typeid(T).name() << " Writing (2) " << size_written_2*sizeof(T) << " bytes at " << write_index << std::endl;
        if (size_written_2)
        {
  //        for(size_t i=write_index; i<write_index + size_written_2; ++i)
  //          std::cout << buffer.data();
  //        std::cout << std::endl;
        }
#endif

        write_index += size_written_2;
        if(write_index == buffer.size())
          write_index = 0;
        c.notify_one();
      }
    }
    size_written += size_written_2;
  }
  
  size_t get_data(T* dst, size_t minimum_size, size_t buffer_size, size_t& copied_items, uint32_t timeout_ms = 0)
  {
    copied_items = 0;
    size_t data_to_read = 0;
    size_t newIndex= read_index;    
    while(1)
    {
      {
        std::unique_lock<std::mutex> l(m);
        read_index += data_to_read;
        if (read_index == buffer.size())
          read_index = 0;
        copied_items += data_to_read;
        if (copied_items >= minimum_size)
          break;
        if(timeout_ms != 0)
        {
          if (!c.wait_for(l, std::chrono::milliseconds(timeout_ms), [this]{return read_index != write_index;}))
          {
#ifdef VERBOSE_TEST
//              std::cout << "Timeout" << std::endl;
#endif
              break;
          }
        }
        else
          c.wait(l, [this]{return read_index != write_index;});

#ifdef VERBOSE_TEST
          std::cout << "Type= "<< typeid(T).name() << " Read index " <<read_index << " write index " << write_index << std::endl;
#endif
        if (write_index < read_index)
          data_to_read = buffer.size() - read_index;
        else
          data_to_read = write_index - read_index;
#ifdef VERBOSE_TEST
        std::cout << "Type= "<< typeid(T).name() << " data_to_read " << data_to_read << " items" << std::endl;
#endif

        if (data_to_read > buffer_size - copied_items)
          data_to_read = buffer_size - copied_items;

        newIndex  = read_index;
      }

      memcpy(dst + copied_items, &buffer[read_index], data_to_read*sizeof(T));

//printf("get 1st elem = %d\n\r", *(&buffer[0]+read_index));
#ifdef VERBOSE_TEST
      std::cout << "Type= "<< typeid(T).name() << " Reading " << data_to_read*sizeof(T) << " bytes at " << read_index << std::endl;
#endif
    }
    return newIndex;
  }

  void resize(size_t new_size)
  {
    std::unique_lock<std::mutex> l(m);
    buffer.resize(new_size);
    write_index = 0 ;
    read_index  = 0 ;
    bufferHasLooped = false;
  }

  void clear(void)
  {
    std::unique_lock<std::mutex> l(m);
    buffer.clear();
    buffer.shrink_to_fit();
    write_index = 0 ;
    read_index  = 0 ;
    bufferHasLooped = false;
  }

  size_t items()
  {
    std::unique_lock<std::mutex> l(m);
    return (write_index + buffer.size() - read_index) % buffer.size();
  }

  size_t size()
  {
    return buffer.size();
  }

  void flush()
  {
    std::unique_lock<std::mutex> l(m);
    write_index = 0 ;
    read_index  = 0 ;
    bufferHasLooped = false;
  }

  bool hasLooped(size_t prev_read_index)
  {
    std::unique_lock<std::mutex> l(m);
    if(read_index < prev_read_index)
      bufferHasLooped = true;
    
    return bufferHasLooped;
  }

  size_t get_data_no_copy(size_t minimum_size, size_t& copied_items, uint32_t timeout_ms = 0)
  {
    size_t buffer_size = minimum_size;
    copied_items = 0;
    size_t data_to_read = 0;
    size_t newIndex= read_index;
    while(1)
    {
      {
        std::unique_lock<std::mutex> l(m);
        read_index += data_to_read;
        if (read_index == buffer.size())
          read_index = 0;
        copied_items += data_to_read;
        if (copied_items >= minimum_size)
          break;
        if(timeout_ms != 0)
        {
          if (!c.wait_for(l, std::chrono::milliseconds(timeout_ms), [this]{return read_index != write_index;}))
          {
              printf("Timeout\n");
              break;
          }
        }
        else
          c.wait(l, [this]{return read_index != write_index;});

        if (write_index < read_index)
          data_to_read = buffer.size() - read_index;
        else
          data_to_read = write_index - read_index;

        if (data_to_read > buffer_size - copied_items)
          data_to_read = buffer_size - copied_items;

        newIndex  = read_index;
      }
      //memcpy(dst + copied_items, &buffer[read_index], data_to_read*sizeof(T));
    }
    return newIndex;
  }

};

#endif /* CIRCULAR_BUFFER_BLOCKING_H__*/
