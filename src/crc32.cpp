#include "crc32.h"
#include <stdio.h>

static uint32_t table[256];

void
crc32_init ()
{
  unsigned int i, j;
  unsigned int c;

  uint32_t poly =
    1 | 1 << 1 | 1 << 2 | 1 << 4 | 1 << 5 | 1 << 7 | 1 << 8 | 1 << 10 | 1 <<
    11 | 1 << 12 | 1 << 16 | 1 << 22 | 1 << 23 | 1 << 26;


  for (i = 0; i < 256; i++)
    {
      for (c = i << 24, j = 8; j > 0; --j)
	c = c & 0x80000000 ? (c << 1) ^ poly : (c << 1);

      table[i] = c;
    }
}

void
crc32_print ()
{
  uint16_t i;
  printf ("table[] =\n{\n");
  for (i = 0; i < 256; i += 4)
    {
      printf ("  0x%08x, 0x%08x, 0x%08x, 0x%08x", table[i + 0], table[i + 1],
	      table[i + 2], table[i + 3]);
      if (i + 4 < 256)
	putchar (',');
      putchar ('\n');
    }
  printf ("};\n");
}

uint32_t
crc32 (const uint32_t init, const uint8_t buf[16])
{
  int8_t i;
  uint32_t crc = init;

  for (i = 0; i < 16; ++i)
    crc = (crc << 8) ^ table[((crc >> 24) ^ buf[i]) & 0xFF];

  return crc;
}

inline uint32_t
crc_8b (const uint32_t init, const uint8_t b)
{
  uint32_t crc = init;

  crc = (crc << 8) ^ table[((crc >> 24) ^ b) & 0xFF];

  return crc;
}
