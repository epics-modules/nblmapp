#include <stdint.h>
#include <math.h>
#include "evdet.h"
#include <iostream>
using namespace std;

void
detect (hls::stream<preprocessedData>& A, hls::stream<eventInfo>& E, hls::stream<eventInfo>& E2, hls::stream<pedestalComputationData>& PC, hls::stream<eventInfoForArchiving>& event_stream, uint16_t neutronTOT_min_indx, uint16_t pileUpTOT_start_indx)
{
#pragma HLS LATENCY min=1 max=1
#pragma HLS PIPELINE II=1
// enabled buffering at port a to improve timing closure
#pragma HLS INTERFACE axis off port=A
#pragma HLS DATA_PACK variable=A
#pragma HLS INTERFACE axis port=E
#pragma HLS DATA_PACK variable=E
#pragma HLS INTERFACE axis port=E2
#pragma HLS DATA_PACK variable=E2
#pragma HLS INTERFACE axis port=event_stream
#pragma HLS INTERFACE axis port=PC
#pragma HLS DATA_PACK variable=PC
#pragma HLS INTERFACE ap_ctrl_none port=return

  static uint32_t event_no;
  static eventInfo eve;         //current event
  static bool bEve = false;     //true if an event has started
  static bool bEveEnd = false;  //true at sample where event ends

  static bool ended_by_frame = false;
  static bool event_isPart2 = false;

  static ap_ufixed<9,9> TOTstartTime;
  static int MTWindx;
  static ap_fixed<17,17> peakValue;
  static ap_ufixed<9,9> peakTime;
  static ap_ufixed<9,9> TOT;
  static bool peakValid;
  static ap_ufixed<9,9> peakCounter;
  static bool TOTvalid;
  static bool pileUp;
  static ap_fixed<26,26> Q_TOT;
  static bool TOTlimitReached;
  static ap_ufixed<13,13> serialNumber;

  static uint8_t positive_saturations;
  static uint8_t negative_saturations;
  static ap_fixed<25,25> Qtotal;

  bool push_event = false;
  eve.newFrame = false;

  preprocessedData twodata;
  A >> twodata;
  
  pedestalComputationData pcd;
  pcd.triggers = twodata.triggers;
  
  pcd.data0.sample = twodata.data0.sample;
  pcd.data1.sample = twodata.data1.sample;
  pcd.data0.inEvent = false;
  pcd.data1.inEvent = false;
  

Input_vector_loop:
  for (int i = 0; i < 2; ++i)
    {
#pragma HLS UNROLL
      preprocessedDataItem data = i ? twodata.data1 : twodata.data0;

      if (data.sample == 0xffff)
         ++positive_saturations;

      if (data.sample == 0x0000)
         ++negative_saturations;

      Qtotal += ap_fixed<25,25>(data.adjusted_sample);

      if (data.belowThr1 || (data.belowThr2 && ended_by_frame))
        {
          //start an event
          if (!bEve)
            {
              MTWindx = data.frame_index;
              bEve = true;
              TOTstartTime = data.sample_index;
              peakValue = data.adjusted_sample;
              peakTime = 0;
              peakValid = false;
              TOTvalid = false;
              pileUp = false;
              TOTlimitReached = false;
              event_isPart2 = ended_by_frame;
              TOT = -1;
              Q_TOT = 0;
              peakCounter = 0;
            }
        }
      else if (!data.belowThr2)
        {
          //end of normal event
          if (bEve)
            {
              bEveEnd = true;
            }
        }

      if (bEve)
        {
#if 1
          if (data.adjusted_sample < peakValue)
            {
              peakValue = data.adjusted_sample;
              peakTime = peakCounter;
            }
#endif
          peakCounter++;
          //TOT
          TOT++;
          //Q_TOT
 
          if (data.peakValid)
            peakValid = true;
          if (TOT >= neutronTOT_min_indx)
            TOTvalid = true;
          if (TOTvalid && peakValid && TOT >= pileUpTOT_start_indx)
            pileUp = true;
        }
      //end event at the edge of MTW

      ended_by_frame = false;
      if (data.newFrame)
      {
        eve.newFrame = true;
    	if (!bEveEnd && bEve && ((TOTvalid && peakValid) || (TOT >= pileUpTOT_start_indx)))
        {
	  ended_by_frame = true;
          TOT++;
          bEveEnd = true;
          TOTlimitReached = true;
        }
      }

      if (ended_by_frame || !bEveEnd)
        Q_TOT += data.adjusted_sample;



          if ((0==i) && bEve)
              pcd.data0.inEvent = true;

          if ((1==i) && bEve)
              pcd.data1.inEvent = true;

      if (bEveEnd)
        {
          bEveEnd = false;
          bEve = false;

          eve.Q_TOT = Q_TOT;
          eve.TOT = TOT;
          eve.TOTvalid = TOTvalid;
          eve.TOTstartTime = TOTstartTime;
          eve.MTWindx = MTWindx;
          eve.peakValue = peakValue;
          eve.peakTime = peakTime;
          eve.peakValid = peakValid;
          eve.TOTlimitReached = TOTlimitReached;
          eve.pileUp = pileUp;
          eve.isPart2 = event_isPart2;
          push_event = true; // at most one event possible from 2 data points
        }
    }

  eve.eventValid = push_event;
  eve.currentwindow = twodata.data0.frame_index;
  
  eve.triggers = twodata.triggers;

  if(twodata.data1.newFrame)
  {
    eve.positive_saturations = positive_saturations;
    eve.negative_saturations = negative_saturations;
    eve.Qtotal = Qtotal;
//    cout << "Qtotal=" << Qtotal << endl;
    positive_saturations = 0;
    negative_saturations = 0;
    Qtotal = 0;
  }  
  E << eve;
  E2 << eve;

  PC << pcd;
  
  eventInfoForArchiving eifa;
  eifa.eventValid = push_event;
  if(push_event)
  {
    eifa.TOTvalid = eve.TOTvalid;
    eifa.peakValid = eve.peakValid;
    eifa.pileUp = eve.pileUp;
    eifa.TOTlimitReached = eve.TOTlimitReached;
    eifa.isPart2 = eve.isPart2;
    eifa.TOT = eve.TOT;
    eifa.Q_TOT = eve.Q_TOT;
    eifa.peakValue =eve.peakValue;
    eifa.TOTstartTime = eve.TOTstartTime;
    eifa.peakTime = eve.peakTime;
    eifa.MTWindx = eve.MTWindx;
    eifa.serialNumber = serialNumber++;
  }

  event_stream << eifa;
}
