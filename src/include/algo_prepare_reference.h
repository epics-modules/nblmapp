#include "data_source.h"
#include "evdet.h"
#include <vector>
using namespace std;

extern vector<eventInfoForArchiving> eventList;
extern vector<neutronCounts> neutronList;
extern vector<preprocessedInfo> sampleList;

void prepare_reference();

#define MTW_PERIODS_IN_TEST_PATTERN 36

