#include <inttypes.h>

void
crc32_init ();
void
crc32_print ();
uint32_t
crc32 (const uint32_t init, const uint8_t buf[16]);
inline uint32_t
crc_8b (const uint32_t init, const uint8_t b);


