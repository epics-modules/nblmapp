#include <stdint.h>
#include <hls_stream.h>
#include "evdet.h"

//#define PUT_TIMESTAMP

void datagen(hls::stream<sampleAndTimestamp>& B, uint32_t nominal_period, bool put_timestamp);
