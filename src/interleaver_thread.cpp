#include <atomic>
#include "buffers.h"
#include "interleaver_thread.h"
#include "data_processor.h"

void interleaver_thread(CircularBufferBlocking<uint8_t>& cb_raw, 
                        CircularBufferBlocking<timestamp_triple>& cb_tri, 
                        CircularBufferBlocking<varianttype> &output,
                        int channel,
                        volatile bool& exitThread,
                        condRead_t &readCB,
                        ifcdaqdrv_usr_t& deviceUser)
{                        
  size_t items_written;
  varianttype myVariant, variantRead;
  size_t copied_tt_items, prev_raw_read_index=0, new_raw_read_index;
  int64_t raw_items_to_read;
  //double t0, t1;
  std::vector<uint8_t> buf, lastBuf;;

  printf("Starting interleaver_thread(CB%d) task\n", channel);
  //t0 = rTime ();  

  while((!exit_loop) && (!exitThread))
  {
    try
    {
      size_t raw_items_read, copied_items;
      // This task adds the timestamp into DOD data. Thus no interleaver_thread() task for the periodic CB 7
      if (channel != cb_channel_periodic)
      {
        // When DOD request, stop writing cb_raw (<=>buffers) and timestamping into output (<=>buffers_timestamped)
        struct timespec timeout;
        int result;
        
        pthread_mutex_lock (&readCB.mutexRetrieveData);
        clock_gettime(CLOCK_REALTIME, &timeout);
        timeout.tv_sec += 1;
        result = 0;
        // Check if DOD has been requested
        while ((readCB.retieveData) && (!exit_loop) && (!exitThread))
        {
            // Acquire last data, then no more data until end of DOD request:
            // No more data are written in the copyDataStream() function until the data retrieving ends.
            raw_items_to_read = cb_raw.items();
            if(raw_items_to_read)
            {
                lastBuf.resize(raw_items_to_read);
                // make a copy in a local buffer for the last data
                cb_raw.get_data(&lastBuf[0], raw_items_to_read, raw_items_to_read, raw_items_read, 0);

                if(cb_raw.hasLooped(prev_raw_read_index) && raw_items_read)
                {
                    varianttype my_variant;
                    while(output.items())
                    {
                        output.get_data(&my_variant, 1, 1, copied_items, 0);
                        if(my_variant.holds_alternative(DATA))
                            break;
                    }
                }

                if(raw_items_read)
                {
                    myVariant.set_DATA((uint32_t)raw_items_read, &lastBuf[0]);
                    output.put_data(&myVariant, 1, items_written);
                    if(items_written != 1)
                        printf("stalled in interleaver_thread() thread, buffers_timestamped is not big enough\n");
                }
            }
            
            // Wait for end of data on demand collecting
            result = pthread_cond_timedwait (&readCB.condRetrieveData, &readCB.mutexRetrieveData, &timeout);
            if (result == ETIMEDOUT)
            {
                clock_gettime(CLOCK_REALTIME, &timeout);
                timeout.tv_sec += 1;
            }
            prev_raw_read_index = 0;
        }
        pthread_mutex_unlock (&readCB.mutexRetrieveData);
        
        int64_t interleaved_block_size;
        if(!read_reg(REG_DECIMATOR_START, deviceUser) && !read_reg(REG_DECIMATOR_STOP, deviceUser))
            // Set big block size when continous acquisition
            interleaved_block_size = INTERLEAVED_BLOCK_SIZE_CONTINOUS_RAWDATA;
        else
            interleaved_block_size = INTERLEAVED_BLOCK_SIZE;

        raw_items_to_read = cb_raw.items();
        if(raw_items_to_read >= interleaved_block_size)
        {
            raw_items_to_read = interleaved_block_size;
            new_raw_read_index = cb_raw.get_data_no_copy(raw_items_to_read, raw_items_read, 0);

            // As "buffers_timestamped" circular buffers can contain more than the "buffers" circular buffers,
            // we must remove each time the oldest "buffers" data in the "buffers_timestamped" once the "buffers" has looped.
            // hasLooped() fct "latches" the loop state. The state is reseted only when cb_raw (<=> buffers) is flushed.
            if(cb_raw.hasLooped(prev_raw_read_index) && raw_items_read)
            {
                varianttype my_variant;
                while(output.items())
                {
                    output.get_data(&my_variant, 1, 1, copied_items, 0);
                    if(my_variant.holds_alternative(DATA))
                        break;
                    //printf("timestamp found\n");
                }
            }
            
            if(raw_items_read)
            {
                myVariant.set_DATA((uint32_t)raw_items_read, &cb_raw.buffer[prev_raw_read_index]);
                output.put_data(&myVariant, 1, items_written);
                if(items_written != 1)
                    printf("stalled in interleaver_thread() thread, buffers_timestamped is not big enough\n");
            }
            prev_raw_read_index = new_raw_read_index;
        }

        copied_tt_items=0;
        timestamp_triple et;
        // retrieve the last timestamp in cb_tri
        while(cb_tri.items())
            cb_tri.get_data(&et, 1, 1, copied_tt_items, 0);

        if(copied_tt_items)
        {
            myVariant.set_TIMESTAMP(1, et);
            output.put_data(&myVariant, 1, items_written);
            if(items_written != 1)
                printf("stalled in interleaver_thread() thread, buffers_timestamped is not big enough\n");
        }
        else
        {
            usleep(INTERLEAVED_TIMESTAMP_STEP); // 1 ms
        }
         
        //t1 = rTime ();
        //printf ("interleaver_thread: make buffers_timestamped after = %.3f ms\n", (t1 - t0) / 1000);
        //t0 = t1;
      }
    } catch(...)
    {
        printf("Exception in interleaver thread\n");
    }
  }
  printf("End of interleaver_thread(CB%d) task\n", channel);
}