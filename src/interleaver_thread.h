#ifndef __INTERLEAVER_THREAD__
#define __INTERLEAVER_THREAD__

#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <signal.h>
#include <string.h>
#include <sys/time.h>
#include <math.h>
#include <thread>
#include <chrono>
//#include <variant>
#include "circular_buffer_blocking.h"
#include "nblmdrv.h"

struct evt_timestamp
{
uint64_t seconds;
uint32_t nanoseconds;
};

struct int_timestamp
{
uint32_t MTW;
uint8_t sample;
};

using cpu_timestamp=timeval;

struct evt_timestamp_pair
{
evt_timestamp e;
cpu_timestamp c;
};

struct int_timestamp_pair
{
int_timestamp i;
cpu_timestamp c;
};

struct timestamp_triple
{
evt_timestamp e;
int_timestamp i;
cpu_timestamp c;
};

///////////////////////////////////////////////////////////
//using varianttype=std::variant<timestamp_triple, std::vector<uint8_t> >;
// tricks to avoid using of variant class (not yet supported in C++14) 
typedef enum {TIMESTAMP, DATA} variantEnum;


class varianttype
{
private:
    variantEnum      varianttag;
    size_t           size;
    uint8_t          *vec;
    timestamp_triple tt;

public:
     varianttype() {};
    ~varianttype() {};



void set_DATA(uint32_t itemsSize, const uint8_t* data)
{
    varianttag=DATA;
    size = itemsSize;
    vec = (uint8_t*)data;
}

void set_TIMESTAMP(uint32_t itemsSize, timestamp_triple& data)
{
    varianttag=TIMESTAMP;
    tt = data; 
    size = sizeof(timestamp_triple);    
}

bool holds_alternative(variantEnum type)
{
     if(type == varianttag)
        return true;
      else
        return false;
}


const timestamp_triple& get_TIMESTAMP(void)
{
    return tt;
}

const uint8_t* get_DATA(void)
{
    return vec;
}

const size_t data_size(void)
{
    return size;
}

};

///////////////////////////////////////////////////////////


void irq_thread(CircularBufferBlocking<int_timestamp_pair>* cb_int, volatile bool& exitThread, ifcdaqdrv_usr_t& deviceUser);
void timestamp_matcher(CircularBufferBlocking<int_timestamp_pair>* cb_int, CircularBufferBlocking<evt_timestamp_pair>* cb_evt, 
                       CircularBufferBlocking<timestamp_triple>* cb_tri, uint32_t channelmask, volatile bool& exitThread);

void camonitorProcessingThread(CircularBufferBlocking<evt_timestamp_pair>* cb_evt, volatile bool& exitThread);

#endif /* __INTERLEAVER_THREAD__ */