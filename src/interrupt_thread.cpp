#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <signal.h>
#include <string.h>
#include <sys/time.h>
#include <math.h>
#include <inttypes.h>

extern "C" {
#include "tsculib.h"
#include "tscioctl.h"
};

#include <thread>
#include "buffers.h"
#include "interleaver_thread.h"

void irq_thread(CircularBufferBlocking<int_timestamp_pair>* cb_int, volatile bool& exitThread, ifcdaqdrv_usr_t& deviceUser)
{
	int irq, ret, count = 0;
	double start, end, delta;
	struct tsc_ioctl_user_irq user_irq = {0};
	user_irq.wait_mode = IRQ_WAIT_INTR | IRQ_WAIT_1S | (5 << 4); // Timeout after 5s
	int tsc_fd = deviceUser.device.tsc_fd;

	printf("Starting irq_thread() task\n");
	irq = 2;
	printf("Trigger line IRQ: Subscribe to user IRQ %d\n", irq);
	user_irq.irq = irq;
	user_irq.mask = 1 << irq;

	double sum = 0;
	double sumsquares = 0;
	double min = 1e10;
	double max = -1e10;

	start = rTime();

	bool firstTime=true;
//        uint64_t lasttrigger;

	while((!exit_loop) && (!exitThread))
	{
		tsc_user_irq_subscribe(tsc_fd, &user_irq);
		ret = tsc_user_irq_wait(tsc_fd, &user_irq);
		if (ret)
		{
			printf("USER IRQ %d (timing irq) timeout\n", user_irq.irq);
		}
		else
		{
		      uint32_t trigger_mtw = read_reg (REG_TRIGGER_MTW, deviceUser);
		      uint32_t trigger_sample = read_reg (REG_TRIGGER_SAMPLE, deviceUser);
//		      uint64_t trigger = (uint64_t)trigger_mtw*250 + trigger_sample;
//		      if(!firstTime)
//		        printf("USER IRQ received, trigger_mtw=%" PRIu32 " trigger_sample=%" PRIu32 " delta=%" PRIu64 "\n", trigger_mtw, trigger_sample, (trigger-lasttrigger)*4);
//		      lasttrigger = trigger;
			if(!firstTime)
				count++;
			struct int_timestamp_pair int_stamp;
			int_stamp.i.MTW=trigger_mtw;
			int_stamp.i.sample=trigger_sample;
			gettimeofday(&int_stamp.c, (struct timezone *) 0);
			size_t size_written;
			cb_int->put_data(&int_stamp, 1, size_written);
			if (size_written!=1)
			  printf("Unable to put int timestamp into FIFO\n");
		}
		
		/* Clear interrupt bit if not auto cleared*/
		//tsc_csr_read(tsc_fd, 0x11DC, &irq);
		//irq = irq & ~user_irq.mask;
		//tsc_csr_write(tsc_fd, 0x11DC, &irq);


	end = rTime ();
	delta = (end - start) / 1000;

	if (!firstTime)
	{
		sum += delta;
		sumsquares += delta*delta;
		if (delta<min)
			min = delta;
		if (delta>max)
			max = delta;

//		printf("USER IRQ: interrupt in %8.3lf ms\n",
//			delta);
	}
	firstTime = false;
	start = end;
	}

	printf("|USER IRQ count=%d | min=%8.3f ms | max=%8.3f ms | avg=%8.3f ms | sigma=%8.3f ms |\n", count, min, max, sum/count, sqrt((sumsquares - (sum*sum) / count) / count) );
	printf("End of irq_thread() task\n");

	return NULL;
}

