#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <math.h>

// This create x axis for histogram
static int createXaxis(aSubRecord *precord) {
    double upLimit       = *(double *)precord->a;   // up limit of histogram
    double lowLimit      = *(double *)precord->b;   // low limit of histogram
    short nbOfElement   = *(short *)precord->c;   // number of elemnt of histogram
 
    // printf("upLimit: %hd\n", upLimit);
    // printf("lowLimit: %hd\n", lowLimit);
    // printf("nbOfElement: %hd\n", nbOfElement);
    // width calculated depending on inputs
    float width = (float)(upLimit - lowLimit) / (float)nbOfElement;
    //printf("width calculated: %f\n", width);
    // x axis created
    epicsFloat32 *xAxis = (epicsFloat32 *)precord->vala;  // output axis
    // calculate new x-axis
    int i = 0;
    for(i=0; i<nbOfElement; i++){
        *xAxis++ = lowLimit + i*width;
    }
    // output (histo width)
    *(float *)precord->valb = width;
    return 0;
}
/* Note the function must be registered at the end. */
epicsRegisterFunction(createXaxis);
