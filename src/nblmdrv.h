#ifndef _NBLMDRV_H_
#define _NBLMDRV_H_ 1

/* Types from the following libraries are used */
#include <stddef.h>
#include <inttypes.h>
#include <stdbool.h>
#include <mutex>
#include "config.h"

#ifdef __cplusplus
extern "C" {
#include <tscioctl.h>
#endif

#define DMA_TURBO
#define EPICS_APP_VERSION    "1.0.71"

#define NB_MAX_OF_IFC                        10
#define CH0                                   0 // It's the CH0_FR (File Reader channel)
#define CH1                                   1
#define CH2                                   2

#define LOSS_PERIOD_NB                      (uint8_t)(7) // Number of period for accumulated losses calculation over each period
#define AM_CHANNEL_NB_MAX                   (uint8_t)(6)// Lodz spec : Max nb of Algorithm Module = Max nb of ADC channel that can be computed
#define NS_TO_DISCRETE(time)                ((uint16_t)time/4) // 250 Mhz => T=4ns
#define DISCRETE_TO_NS(discrete)            (int32_t)((uint16_t)(discrete)*4) // 250 Mhz => T=4ns
#define DISCRETE64_TO_NS(discrete_uint64)   (double)((uint64_t)discrete_uint64*4) // 250 Mhz => T=4ns
#define DISCRETE_TO_US(discrete)            (int32_t)((uint16_t)discrete)   // 1 Mhz => T=1us
#define NBLM_NB_SAMPLES_BY_WINDOW           (uint32_t)(250) // (1us = 1000ns) / 4ns

#define NBLM_BITSTREAM_BUILT_TIME_TAG       (uint8_t)(0x28) // TSCR address register fot the bitstream ID


typedef struct
{
    pthread_cond_t  condRetrieveData;
    pthread_mutex_t mutexRetrieveData;
    volatile bool   retieveData;
    volatile bool   retievePause;
}
condRead_t;

/**
 * @brief Enumeration of possible error codes.
 */

typedef enum {
    status_success          = 0,    /**< Operation successful. */
    status_device_armed     = -1,   /**< The device is armed. */
    status_argument_range   = -2,   /**< Argument out of range. */
    status_argument_invalid = -3,   /**< Invalid argument choice. */
    status_device_access    = -4,   /**< Error in reading/writing device registers. */
    status_read             = -5,   /**< Error in reading device memory. */
    status_write            = -6,   /**< Error in writing to device memory. */
    status_internal         = -7,   /**< Internal library error. */
    status_no_device        = -8,   /**< Device not opened. */
    status_device_state     = -9,   /**< Device in an undefined state. */
    status_irq_error        = -10,  /**< Waiting for irq resulted in error from device. */
    status_irq_timeout      = -11,  /**< Waiting for irq timeout. */
    status_irq_release      = -12,  /**< Waiting for irq was stopped. */
    status_i2c_busy         = -13,  /**< i2c bus busy. */
    status_i2c_nack         = -14,  /**< i2c transfer not acknowledged. */
    status_spi_busy         = -15,  /**< spi interface busy. */
    status_flash_busy       = -16,  /**< Flash chip busy. */
    status_flash_failed     = -17,  /**< Flashing image failed. */
    status_incompatible     = -18,  /**< Incompatible device. */
    status_no_support       = -19,  /**< Unsupported feature. */
    status_fru_info_invalid = -20,  /**< Invalid FRU information. */
    status_buf_len          = -21,  /**< Invalid argument buffer length. */
    status_cancel           = -22,  /**< Operation was canceled */
    status_config           = -23,  /**< Configuration not allowed */
    status_unknown          = -24
} ifcdaqdrv_status;

/**
 * @brief Strings for error codes.
 */

static const char *ifcdaqdrv_errstr[] =
{
    "operation successful",
    "the device is armed",
    "argument out of range",
    "invalid argument choice",
    "error reading/writing device registers",
    "error reading device memory",
    "error writing to device memory",
    "internal library error",
    "device not opened",
    "device in an undefined state",
    "irq wait failed",
    "irq timeout",
    "irq stopped",
    "i2c busy",
    "i2c not acknowledged",
    "spi busy",
    "flash busy",
    "flashing image failed",
    "incompatible device",
    "unsupported feature",
    "invalid FRU information",
    "invalid argument buffer length",
    "acquisition canceled",
    "configuration not allowed",

    "unknown error code"
};

/**
 * @brief Return String interpretation of error code.
 */

static inline const char *ifcdaqdrv_strerror(ifcdaqdrv_status status) {
    if (status < status_unknown || status > 0) {
        status = status_unknown;
    }
    return ifcdaqdrv_errstr[-status];
}

/**
 * @brief Library user context struct.
 */

struct ifcdevice_t {
    int                tsc_fd;
    uint8_t*           Kbuffer[2][CB_CHANNEL_NB_MAX];
    tsc_ioctl_kbuf_req tsc_kbuf_req[2][CB_CHANNEL_NB_MAX];
    tsc_ioctl_dma_req  dma_req[2];
};

typedef struct ifcdevice_t ifcdevice_t;


struct ifcdaqdrv_usr_t {
    uint32_t     card;       /**< Card/Crate number corresponding to PCIe slot. */
    ifcdevice_t  device;     /**< Device private context.  */
    std::string  hdf5Name;
    size_t       DOD_MemSize;
};

typedef struct ifcdaqdrv_usr_t ifcdaqdrv_usr_t;

void read_versions(char *bitstream_date, ifcdaqdrv_usr_t& deviceUser);

extern ifcdaqdrv_usr_t deviceUserTab[NB_MAX_OF_IFC];;

#ifdef __cplusplus
}
#endif
#endif
