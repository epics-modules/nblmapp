#include <stdint.h>
#include <math.h>
#include "evdet.h"

#include <iostream>
using namespace std;


void
neutron_aligner (hls::stream<eventInfo>& E, hls::stream<eventInfo>& New, hls::stream<eventInfo>& Old)
{
#pragma HLS PIPELINE II=1
#pragma HLS LATENCY min=1 max=1
#pragma HLS INTERFACE axis off port=E
#pragma HLS DATA_PACK variable=E
#pragma HLS INTERFACE axis port=New
#pragma HLS DATA_PACK variable=New
#pragma HLS INTERFACE axis port=Old
#pragma HLS DATA_PACK variable=Old
#pragma HLS INTERFACE ap_ctrl_none port=return

static eventInfo old_event = {0};
eventInfo new_event;
E >> new_event;

New << new_event;
Old << old_event;
if (new_event.eventValid)
	old_event = new_event;
}
