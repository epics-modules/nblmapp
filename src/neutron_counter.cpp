#include <stdint.h>
#include <math.h>
#include <ap_fixed.h>
#include "evdet.h"

#include <iostream>
#ifndef __SYNTHESIS__
#include <fstream>
#endif

using namespace std;



void doNeutronCount_eve(const eventInfo &eve,const eventInfo &eve_old,neutronCounts &nc_eve, ap_ufixed<32,0> inverse_of_Q_TOT_single_neutron)
//determines the neutron counts for event eve (either with counting or charged method) and
//stores the result in nc_eve (for counting method in nc_eve.N_n and for charge method
//in nc_eve.N_qtot].
//eve_old = previous event
{
  //isSplit_part2=true: event is second part of a neutron/pileup
  //signal that was split at MTW
  bool isSplit_part2=false;
  if(eve_old.TOTvalid && eve_old.peakValid && eve_old.TOTlimitReached && eve.isPart2)
    isSplit_part2=true;

  int method=0; //1 for counting, 2 for Q method
  if(isSplit_part2)
    method=2;
  else{
    if(eve.TOTvalid && eve.peakValid){
      if(eve.pileUp) //pile up
	method=2;
      else{ //single neutron
	if(eve.TOTlimitReached)
	  method=2;
	else
	  method=1;
      }
    }
  }

  if(method==1)
    nc_eve.N_n=1;
  else if(method==2){
//	  cout << eve.Q_TOT*inverse_of_Q_TOT_single_neutron << endl;
//	  cout << ap_fixed<33,32>(eve.Q_TOT*inverse_of_Q_TOT_single_neutron) << endl;
//	  cout << ap_fixed<33,32>(eve.Q_TOT*inverse_of_Q_TOT_single_neutron) + ap_fixed<33,32>(0.5) << endl;
	  nc_eve.N_qtot=ap_fixed<33,32>(-eve.Q_TOT*inverse_of_Q_TOT_single_neutron) + ap_fixed<33,32>(0.5); // result in one-hundredths of neutron
  }

}


void
neutron_counter (hls::stream<eventInfo>& New, hls::stream<eventInfo>& Old, hls::stream<neutronCounts>& N, ap_ufixed<32,0> inverse_of_Q_TOT_single_neutron /* pre-multiplied by 100 */)
{
#pragma HLS PIPELINE II=1
#pragma HLS LATENCY max=4
#pragma HLS INTERFACE axis off port=New
#pragma HLS DATA_PACK variable=New
#pragma HLS INTERFACE axis off port=Old
#pragma HLS DATA_PACK variable=Old
#pragma HLS INTERFACE axis port=N
#pragma HLS DATA_PACK variable=N
#pragma HLS INTERFACE ap_ctrl_none port=return

eventInfo old_event;
Old >> old_event;
eventInfo new_event;
New >> new_event;
neutronCounts nc;

nc.dataValid = false;

nc.Q_background = 0;

if(new_event.eventValid)
{
	nc.N_n = 0;
	nc.N_qtot = 0;
	doNeutronCount_eve(new_event, old_event, nc, inverse_of_Q_TOT_single_neutron);
        nc.dataValid = true;

        //bckg info
        bool isBckg=false;
        if(new_event.isPart2){
          if((old_event.TOTvalid && !old_event.peakValid) & !(new_event.TOTvalid && new_event.peakValid))
            isBckg=true;
        }
        else {
        if(new_event.TOTvalid && !new_event.peakValid)
           isBckg=true;}
        if(isBckg)
          nc.Q_background = new_event.Q_TOT;


#ifndef __SYNTHESIS__
    if (new_event.eventValid)
    {

        CB1 << '\t';
        CB1 << (int)new_event.pileUp << '\t';
        CB1 << (int)new_event.peakValid << '\t';
        CB1 << (int)new_event.TOTvalid << '\t';
        CB1 << (int)    new_event.TOTlimitReached << '\t';
        CB1 << (int)    new_event.isPart2 << '\t';
        CB1 << new_event.TOTstartTime << '\t';
        CB1 << new_event.TOT << '\t';
        CB1 << new_event.peakTime << '\t';
        CB1 << (double)new_event.peakValue / counts_per_V << '\t';
        CB1 << (double)new_event.Q_TOT / counts_per_V << '\t';
        CB1 << new_event.MTWindx << '\t';
        CB1 << (uint32_t)nc.N_n << '\t';
        CB1 << (double)nc.N_qtot / 100 << '\n';
    }
#endif

}

nc.newFrame = new_event.newFrame;
nc.positive_saturations = new_event.positive_saturations;
nc.negative_saturations = new_event.negative_saturations;
nc.Qtotal = new_event.Qtotal;

nc.triggers = new_event.triggers;

//if(nc.newFrame || nc.dataValid)
//{
	N << nc;
//}

}
