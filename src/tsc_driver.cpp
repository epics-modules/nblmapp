#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/types.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <errno.h>
#include <getopt.h>
#include <stdint.h>
#include <pthread.h>
#include <inttypes.h>
#include <signal.h>
#include <thread>
#include <mutex>

extern "C"
{
#include <tscioctl.h>
#include <tsculib.h>
}

#include "u256.h"

#include "data_source.h"
#include "evdet.h"

#include "buffers.h"
#include "nblmdrv.h"

#ifdef __x86_64__
#define PCIE_SPACE DMA_SPACE_PCIE1
#else
#define PCIE_SPACE DMA_SPACE_PCIE
//#error error
#endif

#ifndef DMA_TURBO
std::mutex dma_mutex;
#else
std::mutex dma_mutex[2];
#endif

#ifdef DMA_TURBO
void
set_turbo_dma_mode(ifcdaqdrv_usr_t& deviceUser)
{
  int tsc_fd = deviceUser.device.tsc_fd;
  struct tsc_ioctl_dma_mode  mode;
  uint32_t data;

  for(int chan=0; chan<4;++chan)
  {
    mode.chan = chan;
    mode.op = DMA_MODE_GET;
    tsc_dma_mode(tsc_fd, &mode);
    mode.mode |= DMA_MODE_TURBO;
    mode.chan = chan;
    mode.op = DMA_MODE_SET;
    tsc_dma_mode(tsc_fd, &mode);
  }
  
  //set arbitration mode
  data = read_reg(0x800-0x1000, deviceUser);
  data &= 0xffffffcf;
  data |= 0x00000030;
//  printf("data=%08x\n",data);
  write_reg(0x800-0x1000, data, deviceUser);

  data = read_reg(0xc00-0x1000, deviceUser);
  data &= 0xffffffcf;
  data |= 0x00000030;
  write_reg(0xc00-0x1000, data, deviceUser);
  
  
  return( 0);

}
#endif

void
write_reg (unsigned reg_offset, uint32_t value, ifcdaqdrv_usr_t& deviceUser)
{
  struct tsc_ioctl_csr_op csr_op;
  int cmd = TSC_IOCTL_CSR_WR;
  csr_op.offset = reg_offset + 0x1000;
  csr_op.data = value;

  int tsc_fd = deviceUser.device.tsc_fd;
  int retval = ioctl (tsc_fd, cmd, &csr_op);
  if (retval < 0)
    {
      perror ("Error writing register");
    }
}

uint32_t
read_reg (unsigned reg_offset, ifcdaqdrv_usr_t& deviceUser)
{
  struct tsc_ioctl_csr_op csr_op;
  int cmd = TSC_IOCTL_CSR_RD;
  int tsc_fd = deviceUser.device.tsc_fd;

  csr_op.offset = reg_offset + 0x1000;
  int retval = ioctl (tsc_fd, cmd, &csr_op);
  if (retval < 0)
    perror ("Error reading register");
  uint32_t v = csr_op.data;
  return v;
}


void
initialize_kernel_dma_buffers (ifcdaqdrv_usr_t& deviceUser)
{
  int i, j;
  {
    int tsc_fd = deviceUser.device.tsc_fd;

    for (j = 0; j < 2; ++j)
      for (i = 0; i < CB_CHANNEL_NB_MAX; ++i)
      {
        {
          deviceUser.device.tsc_kbuf_req[j][i].size = KBUF_SIZE;
          int res;
          if ((res = tsc_kbuf_alloc (tsc_fd, &(deviceUser.device.tsc_kbuf_req[j][i]))))
          {
            perror ("Cannot allocate kernel buffers\n");
            exit (EXIT_FAILURE);
          }
          deviceUser.device.Kbuffer[j][i] = (uint8_t *) tsc_kbuf_mmap (tsc_fd, &(deviceUser.device.tsc_kbuf_req[j][i]));
          if (!deviceUser.device.Kbuffer[j][i])
            perror ("tsc_kbuf_mmap");
          //printf("Alloc Kbuffer[%d][%d]\n", j, i);
        }
      }
  }
}

void
free_kernel_dma_buffers (ifcdaqdrv_usr_t& deviceUser)
{
  int i, j;

  {
    int tsc_fd = deviceUser.device.tsc_fd;

    for (j = 0; j < 2; ++j)
      for (i = 0; i < CB_CHANNEL_NB_MAX; ++i)
        {
          //fprintf (stderr, "unmap\n");
          tsc_kbuf_munmap (&(deviceUser.device.tsc_kbuf_req[j][i]));
          tsc_kbuf_free (tsc_fd, &(deviceUser.device.tsc_kbuf_req[j][i]));
          //printf("Free Kbuffer[%d][%d]\n", j, i);
        }
  }
  fprintf (stderr, "Kernel buffers freed\n");
}

int
initialize_driver (ifcdaqdrv_usr_t& deviceUser)
{
  char deviceName[100];

  printf("Initialize device %s%d\n","/dev/bus/bridge/tsc_ctl_central", deviceUser.card);
  sprintf(deviceName,"%s%d","/dev/bus/bridge/tsc_ctl_central", deviceUser.card);

  int tsc_fd = open (deviceName, O_RDWR);
  deviceUser.device.tsc_fd = tsc_fd;

  if (tsc_fd < 0)
  {
    perror (deviceName);
    return -1;
  }
  
  initialize_kernel_dma_buffers (deviceUser);

  deviceUser.device.dma_req[0].src_space = DMA_SPACE_SHM;
  deviceUser.device.dma_req[0].src_addr = 0;		//placeholder
  deviceUser.device.dma_req[0].src_mode = DMA_PCIE_TC0;

  deviceUser.device.dma_req[0].des_space = PCIE_SPACE;
  deviceUser.device.dma_req[0].des_addr = deviceUser.device.tsc_kbuf_req[0][0].b_base;
  deviceUser.device.dma_req[0].des_mode = DMA_PCIE_TC0;

#ifdef DMA_TURBO
  deviceUser.device.dma_req[0].start_mode = 0;
  deviceUser.device.dma_req[0].end_mode = 0;
  deviceUser.device.dma_req[0].wait_mode = 0;
  deviceUser.device.dma_req[0].size = BUFSIZE;
  deviceUser.device.dma_req[0].intr_mode = 0;
#else
  deviceUser.device.dma_req[0].start_mode = (char) DMA_START_CHAN (DMA_CHAN_0);
  deviceUser.device.dma_req[0].end_mode = 0;
  deviceUser.device.dma_req[0].wait_mode = DMA_WAIT_INTR | DMA_WAIT_1S | (5 << 4);	/* 5 sec timeout */
  deviceUser.device.dma_req[0].size = BUFSIZE;
  deviceUser.device.dma_req[0].intr_mode = DMA_INTR_ENA;
#endif

#ifndef DMA_TURBO
  tsc_dma_free (tsc_fd, DMA_CHAN_0);	// in case previous program execution did not clean up

  if (tsc_dma_alloc (tsc_fd, DMA_CHAN_0))
  {
    printf ("Cannot allocate DMA on channel #%d -> %s\n", DMA_CHAN_0, strerror (errno));
    return 1;
  }
#endif
  deviceUser.device.dma_req[1].src_space = DMA_SPACE_SHM2;
  deviceUser.device.dma_req[1].src_addr = 0;		//placeholder
  deviceUser.device.dma_req[1].src_mode = DMA_PCIE_TC0;

  deviceUser.device.dma_req[1].des_space = PCIE_SPACE;
  deviceUser.device.dma_req[1].des_addr = deviceUser.device.tsc_kbuf_req[1][0].b_base;
  deviceUser.device.dma_req[1].des_mode = DMA_PCIE_TC0;

#ifdef DMA_TURBO
  deviceUser.device.dma_req[1].start_mode = 0;
  deviceUser.device.dma_req[1].end_mode = 0;
  deviceUser.device.dma_req[1].wait_mode = 0;
  deviceUser.device.dma_req[1].size = BUFSIZE;
  deviceUser.device.dma_req[1].intr_mode = 0;
#else
  deviceUser.device.dma_req[1].start_mode = (char) DMA_START_CHAN (DMA_CHAN_2);
  deviceUser.device.dma_req[1].end_mode = 0;
  deviceUser.device.dma_req[1].wait_mode = DMA_WAIT_INTR | DMA_WAIT_1S | (5 << 4);	/* 5 sec timeout */
  deviceUser.device.dma_req[1].size = BUFSIZE;
  deviceUser.device.dma_req[1].intr_mode = DMA_INTR_ENA;

  tsc_dma_free (tsc_fd, DMA_CHAN_2);	// in case previous program execution did not clean up
#endif

#ifndef DMA_TURBO
  if (tsc_dma_alloc (tsc_fd, DMA_CHAN_2))
  {
    printf ("Cannot allocate DMA on channel #%d -> %s\n", DMA_CHAN_2, strerror (errno));
    return 1;
  }
#endif
  int word = 0xc0000000;
  if (tsc_pon_write (tsc_fd, 0x0c, &word))
  {
    printf("writePonReg: write register 'mezzanine'  with data=%x failed\n",	 word);
  }

  return 0;
}

void
finalize_driver (ifcdaqdrv_usr_t& deviceUser)
{
  //  printf("Finalizing driver\n");
  int tsc_fd = deviceUser.device.tsc_fd;

  if(tsc_fd)
  {
    free_kernel_dma_buffers (deviceUser);
    tsc_dma_free (tsc_fd, DMA_CHAN_0);
    tsc_dma_free (tsc_fd, DMA_CHAN_2);
    close(tsc_fd);
  }
  deviceUser.device.tsc_fd = 0;
  printf("Driver for the PCIe slot %d is closed\n", deviceUser.card);
}

void finalize_all_devices(void)
{
  uint8_t i=0;
  while(deviceUserTab[i].card != 0)
  {
    finalize_driver(deviceUserTab[i]);
    deviceUserTab[i].card = 0;
    i++;
  }
}

int dma_transfer_to_shm(int mem, void* buf, uint32_t offset, uint32_t size, ifcdaqdrv_usr_t& deviceUser)
{
  struct tsc_ioctl_dma_req dma_req;
  int tsc_fd = deviceUser.device.tsc_fd;

  memcpy(deviceUser.device.Kbuffer[0][0], buf, size);

  dma_req.des_addr = offset;
  if (mem == 0)
    dma_req.des_space = DMA_SPACE_SHM;
  else
    dma_req.des_space = DMA_SPACE_SHM2;
  dma_req.src_mode = DMA_PCIE_TC0;

  dma_req.src_space = PCIE_SPACE;
  dma_req.src_addr = deviceUser.device.tsc_kbuf_req[0][0].b_base;
  dma_req.src_mode = DMA_PCIE_TC0;

#ifdef DMA_TURBO
  dma_req.start_mode = 0;
  dma_req.end_mode = 0;
  dma_req.wait_mode = 0;
  dma_req.size = size;
  dma_req.intr_mode = 0;

  int retval = tsc_dma_transfer (tsc_fd, &dma_req);
  
  uint32_t valid_status;
  
  if (mem == 0)
    valid_status = (0x2 << 28) | DMA_STATUS_DONE | DMA_STATUS_ENDED | DMA_STATUS_RUN_WR0;
  else
    valid_status = (0x3 << 28) | DMA_STATUS_DONE | DMA_STATUS_ENDED | DMA_STATUS_RUN_WR0;
#else
  if(mem == 0)
    dma_req.start_mode = (char) DMA_START_CHAN (DMA_CHAN_0);
  else
    dma_req.start_mode = (char) DMA_START_CHAN (DMA_CHAN_2);
  
  dma_req.end_mode = 0;
  dma_req.wait_mode = DMA_WAIT_INTR | DMA_WAIT_1S | (5 << 4);	/* 5 sec timeout */
  dma_req.size = size;
  dma_req.intr_mode = DMA_INTR_ENA;

  int retval = tsc_dma_move (tsc_fd, &dma_req);

#endif

  if (retval < 0)
    {
      printf ("Cannot perform DMA transfer on channel #%d -> %s\n",
	      mem, strerror (errno));
      return -1;
    }

#if 0
  if (dma_req.dma_status != valid_status)
    {
      printf ("Invalid status on channel #%d -> is %x should be %x\n",
	      mem, dma_req.dma_status, valid_status);
//      return -1;
    }
#endif

  return 0;  
  
}

std::mutex dma_mutex1;

int dma_transfer_from_shm(int mem, void* buf, uint32_t offset, uint32_t size, ifcdaqdrv_usr_t& deviceUser)
{
std::lock_guard<std::mutex> lock(dma_mutex1);

  struct tsc_ioctl_dma_req dma_req;
  int tsc_fd = deviceUser.device.tsc_fd;

  dma_req.src_addr = offset;
  if (mem == 0)
    dma_req.src_space = DMA_SPACE_SHM;
  else
    dma_req.src_space = DMA_SPACE_SHM2;
  dma_req.src_mode = DMA_PCIE_TC0;

  dma_req.des_space = PCIE_SPACE;
  dma_req.des_addr = deviceUser.device.tsc_kbuf_req[mem][0].b_base;
  dma_req.des_mode = DMA_PCIE_TC0;

#ifdef DMA_TURBO
  dma_req.start_mode = 0;
  dma_req.end_mode = 0;
  dma_req.wait_mode = 0;
  dma_req.size = size;
  dma_req.intr_mode = 0;

  int retval = tsc_dma_transfer (tsc_fd, &dma_req);

  uint32_t valid_status;
  
  if (mem == 0)
    valid_status = (0x2 << 28) | DMA_STATUS_DONE | DMA_STATUS_ENDED | DMA_STATUS_RUN_RD0;
  else
    valid_status = (0x3 << 28) | DMA_STATUS_DONE | DMA_STATUS_ENDED | DMA_STATUS_RUN_RD0;
#else
  if(mem == 0)
    dma_req.start_mode = (char) DMA_START_CHAN (DMA_CHAN_0);
  else
    dma_req.start_mode = (char) DMA_START_CHAN (DMA_CHAN_2);
  
  dma_req.end_mode = 0;
  dma_req.wait_mode = DMA_WAIT_INTR | DMA_WAIT_1S | (5 << 4);	/* 5 sec timeout */
  dma_req.size = size;
  dma_req.intr_mode = DMA_INTR_ENA;

  int retval = tsc_dma_move (tsc_fd, &dma_req);  
#endif

  if (retval < 0)
    {
      printf ("Cannot perform DMA transfer on channel #%d -> %s\n",
	      mem, strerror (errno));
      return -1;
    }

#if 0
  if (dma_req.dma_status != valid_status)
    {
      printf ("Invalid status on channel #%d -> is %x should be %x\n",
	      mem, dma_req.dma_status, valid_status);
      return -1;
    }
#endif
  memcpy(buf, deviceUser.device.Kbuffer[mem][0], size);  
  return 0;  
  
}


int
transfer_dma (int ch, int size, int t, uint32_t readAddress, ifcdaqdrv_usr_t& deviceUser, int current_buf)
{
int tsc_fd = deviceUser.device.tsc_fd;
#ifndef DMA_TURBO
std::lock_guard<std::mutex> lock(dma_mutex);
#else
std::lock_guard<std::mutex> lock(dma_mutex[t]);
#endif

#if 1
  if (channels_in_cb[0] & (1u << ch))
    deviceUser.device.dma_req[t].src_space = DMA_SPACE_SHM;
  else
    deviceUser.device.dma_req[t].src_space = DMA_SPACE_SHM2;
  
  deviceUser.device.dma_req[t].src_mode = DMA_PCIE_TC0;

  deviceUser.device.dma_req[t].des_space = PCIE_SPACE;
  deviceUser.device.dma_req[t].des_mode = DMA_PCIE_TC0;

//  if (channels_in_thread[0] & (1u << ch))
#ifdef DMA_TURBO
  deviceUser.device.dma_req[t].start_mode = 0;  
  deviceUser.device.dma_req[t].end_mode = 0;
  deviceUser.device.dma_req[t].wait_mode = 0;
  deviceUser.device.dma_req[t].size = BUFSIZE;
  deviceUser.device.dma_req[t].intr_mode = 0;
#else
  if (t == 0)
    deviceUser.device.dma_req[t].start_mode = (char) DMA_START_CHAN (DMA_CHAN_0);
  else
    deviceUser.device.dma_req[t].start_mode = (char) DMA_START_CHAN (DMA_CHAN_2);  
  
  deviceUser.device.dma_req[t].end_mode = 0;
  deviceUser.device.dma_req[t].wait_mode = DMA_WAIT_INTR | DMA_WAIT_1S | (5 << 4);	/* 5 sec timeout */
  deviceUser.device.dma_req[t].size = BUFSIZE;
  deviceUser.device.dma_req[t].intr_mode = DMA_INTR_ENA;
#endif
#endif

  int retval;
  deviceUser.device.dma_req[t].src_addr = readAddress;
  deviceUser.device.dma_req[t].des_addr = deviceUser.device.tsc_kbuf_req[current_buf][ch].b_base;
#ifndef DMA_TURBO
  deviceUser.device.dma_req[t].size = size | DMA_SIZE_PKT_128;
  
  retval = tsc_dma_move (tsc_fd, &deviceUser.device.dma_req[t]);
#else
  deviceUser.device.dma_req[t].size = ((size + 1023)/1024)*1024;
  retval = tsc_dma_transfer (tsc_fd, &deviceUser.device.dma_req[t]);

  uint32_t valid_status;
  
  if (t == 0)
    valid_status = (0x2 << 28) | DMA_STATUS_DONE | DMA_STATUS_ENDED | DMA_STATUS_RUN_RD0;
  else
    valid_status = (0x3 << 28) | DMA_STATUS_DONE | DMA_STATUS_ENDED | DMA_STATUS_RUN_RD0;
#endif




  if (retval < 0)
    {
      printf ("Cannot perform DMA transfer on channel #%d -> %s\n",
	      t, strerror (errno));
      return -1;
    }
#if 0
  if (deviceUser.device.dma_req[t].dma_status != valid_status)
    {
      printf ("Invalid status on channel #%d -> is %x should be %x\n",
	      t, deviceUser.device.dma_req[t].dma_status, valid_status);
      return -1;
    }
#endif 

  return 0;
}

int
wait_for_interrupt ()
{
  return 0;
}

/*
 * Read/Display the bitstream built time
 *
 */
void read_versions(char *bitstream_date, ifcdaqdrv_usr_t& deviceUser) {
	uint8_t mm      = 0;
	uint8_t dd      = 0;
	uint8_t yy      = 0;
	uint8_t hh      = 0;
	uint8_t mn      = 0;
	uint8_t ss      = 0;
  const char  *month[16] = {"Err","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec","Err","Err","Err"};
  uint32_t bitstream_builtTime;


    bitstream_builtTime = read_reg ((unsigned)(NBLM_BITSTREAM_BUILT_TIME_TAG)-0x1000, deviceUser);
    ss = bitstream_builtTime & 0x3f;
	  mn = (bitstream_builtTime>>6) & 0x3f;
	  hh = (bitstream_builtTime>>12) & 0x1f;
	  yy = (bitstream_builtTime>>17) & 0x3f;
	  mm = (bitstream_builtTime>>23) & 0xf;
	  dd = (bitstream_builtTime>>27) & 0x1f;
    sprintf(bitstream_date, "%s %02d 20%02d %02d:%02d:%02d", month[mm], dd, yy, hh, mn, ss); 
}
