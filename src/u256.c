#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include "u256.h"

void
u256_shift_in (struct u256 *d, uint8_t n[16])
{
  d->d[3] = d->d[1];
  d->d[2] = d->d[0];
  d->d[1] =
    ((uint64_t) (n[0]) << 56) | ((uint64_t) (n[1]) << 48) | ((uint64_t) (n[2]) <<
							 40) |
    ((uint64_t) (n[3]) << 32) | ((uint64_t) (n[4]) << 24) | ((uint64_t) (n[5]) <<
							 16) |
    ((uint64_t) (n[6]) << 8) | ((uint64_t) (n[7]) << 0);
  d->d[0] =
    ((uint64_t) (n[8]) << 56) | ((uint64_t) (n[9]) << 48) | ((uint64_t) (n[10]) <<
							 40) |
    ((uint64_t) (n[11]) << 32) | ((uint64_t) (n[12]) << 24) | ((uint64_t) (n[13]) <<
							   16) |
    ((uint64_t) (n[14]) << 8) | ((uint64_t) (n[15]) << 0);
  d->bits += 128;
}

uint64_t
u256_get_bitfield (struct u256 *d, uint8_t maxindex, uint8_t minindex)
{
  uint8_t topword = maxindex / 64;
  uint8_t bottomword = minindex / 64;
  uint8_t topindex = maxindex % 64;
  uint8_t bottomindex = minindex % 64;
  uint8_t bits = maxindex - minindex + 1;
  uint64_t result;
  
  if(topword == bottomword)
    result = (d->d[topword] >> bottomindex) & ((1ull << bits) - 1);
  else
  {
  uint8_t bits_in_lowword = 64 - bottomindex;
  uint64_t toppart= (d->d[topword] & ((1ull << (topindex + 1)) - 1)) << (bits_in_lowword);
  result = toppart | (d->d[bottomword] >> (bottomindex));
  }
  return result;
}

void
u256_shift_right (struct u256 *d, unsigned bits /* less than 64 */ )
{
  int i;
  for (i = 0; i < 4; ++i)
    {
      d->d[i] >>= bits;
      if (i < 3)
	d->d[i] |= d->d[i + 1] << (64 - bits);
    }
  d->bits -= bits;
}

uint64_t
u256_get_top_bits_and_cut (struct u256 *d, int bits)
{
  uint64_t result = u256_get_bitfield (d, d->bits - 1, d->bits - bits);
  d->bits -= bits;
  return result;
}

#ifdef TEST

void
process_event (struct u256 *d, int ch)
{
  uint32_t MTWindx = u256_get_top_bits_and_cut (d, 32);// 32 bits
  int32_t Q_TOT = u256_get_top_bits_and_cut (d, 26);// 26 bits 
  uint32_t TOT = u256_get_top_bits_and_cut (d, 9);// 9 bits
  int8_t TOTlimitReached = u256_get_top_bits_and_cut (d, 1);// 1 bit
  uint32_t TOTstartTime = u256_get_top_bits_and_cut (d, 9);// 9 bits 
  uint8_t TOTvalid = u256_get_top_bits_and_cut (d, 1);// 1 bit
  uint8_t isPart2= u256_get_top_bits_and_cut (d, 1);// 1 bit
  uint32_t peakTime = u256_get_top_bits_and_cut (d, 9);// 9 bits
  uint8_t peakValid = u256_get_top_bits_and_cut (d, 1);// 1 bit
  int32_t peakValue = u256_get_top_bits_and_cut (d, 17);// 17 bits
  uint8_t pileUp = u256_get_top_bits_and_cut (d, 1);// 1 bit
  uint32_t serialNumber = u256_get_top_bits_and_cut (d, 13); // 13 bits
  printf("Channel %d MTWindx=%x Q_TOT=%x TOT=%u event_serial_Number %u\n", ch, MTWindx, Q_TOT, TOT, serialNumber);
}

int main()
{
struct u256 s = {0};

s.d[1] = 0x80b410f99ddb17e2ull;
s.d[0] = 0xc280000000000000ull;
s.bits = 128;

printf("%016" PRIx64 "%016" PRIx64 "\n", s.d[1], s.d[0]);
process_event(&s,1);


s.d[3]=0x7ff67ee7fd9fb800ull;
s.d[2]=0x000000b4aac2887full;
s.d[1]=0xffff67ee0003b800ull;
s.d[0]=0x000000b4aac2887full;
s.bits = 135;
uint64_t v = u256_get_top_bits_and_cut (&s, 26 + 14);
printf("v=%016" PRIx64 "\n",v);

return 0;
}

#endif