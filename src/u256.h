#ifndef __U256_H__
#define __U256_H__
#include <stdint.h>

struct u256
{
  uint64_t d[4];		// 3 -MSW, 0 - LSW
  unsigned bits;
};


#ifdef __cplusplus
extern "C" {
#endif

void u256_shift_in (struct u256 *d, uint8_t n[16]);
uint64_t u256_get_bitfield (struct u256 *d, uint8_t maxindex, uint8_t minindex);
uint64_t u256_get_top_bits_and_cut (struct u256 *d, int bits);

#ifdef __cplusplus
}
#endif

#endif /* __U256_H__ */